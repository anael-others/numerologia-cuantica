<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_GB">
<context>
    <name>Form</name>
    <message>
        <location filename="../Interface/form.ui" line="20"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_form.h" line="280"/>
        <source>Página principal - Numerología Cuántica</source>
        <translation>Main page - Quantum numerology</translation>
    </message>
    <message>
        <location filename="../Interface/form.ui" line="52"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_form.h" line="282"/>
        <source>DATOS PERSONALES:</source>
        <translation>PERSONAL INFO:</translation>
    </message>
    <message>
        <location filename="../Interface/form.ui" line="87"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_form.h" line="285"/>
        <source>dddd, dd &apos;de&apos; MMMM &apos;de&apos; yyyy</source>
        <translation>dddd, MMMM dd, yyyy</translation>
    </message>
    <message>
        <location filename="../Interface/form.ui" line="61"/>
        <location filename="../Interface/form.ui" line="97"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_form.h" line="283"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_form.h" line="286"/>
        <source>Fecha de nacimiento:</source>
        <translation>Birthdate:</translation>
    </message>
    <message>
        <location filename="../Interface/form.ui" line="121"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_form.h" line="287"/>
        <source>Nombre completo:</source>
        <translation>Long name:</translation>
    </message>
    <message>
        <location filename="../Interface/form.ui" line="231"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_form.h" line="290"/>
        <source>Apellidos:</source>
        <translation>Surname:</translation>
    </message>
    <message>
        <location filename="../Interface/form.ui" line="244"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_form.h" line="291"/>
        <source>La persona utiliza un nombre activo</source>
        <translation>The person usually uses a different forename</translation>
    </message>
    <message>
        <location filename="../Interface/form.ui" line="152"/>
        <location filename="../Interface/form.ui" line="184"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_form.h" line="288"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_form.h" line="289"/>
        <source>/</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../Interface/form.ui" line="68"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_form.h" line="284"/>
        <source>Nombre activo:</source>
        <translation>Forename:</translation>
    </message>
    <message>
        <location filename="../Interface/form.ui" line="288"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_form.h" line="292"/>
        <source>Continuar</source>
        <translation>Continue</translation>
    </message>
    <message>
        <location filename="../Interface/form.ui" line="356"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_form.h" line="281"/>
        <source>Abrir</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="51"/>
        <source>Atrás</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="54"/>
        <source>Nuevo</source>
        <translation>New</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="55"/>
        <source>Configuración</source>
        <translation>Settings</translation>
    </message>
    <message>
        <source>ERROR</source>
        <translation type="vanished">ERROR</translation>
    </message>
    <message>
        <source>Error al sobreescribir la configuración del programa.
No se pueden modificar los permisos del archivo.</source>
        <translation type="vanished">Error overwriting program configuration.
File permissions can&apos;t be modified.</translation>
    </message>
    <message>
        <source>Código de error: %1</source>
        <translation type="vanished">Error code: %1</translation>
    </message>
    <message>
        <source>Error al leer el archivo de configuración</source>
        <translation type="vanished">Error reading configuration file</translation>
    </message>
    <message>
        <source>Se ha producido un error al intentar cargar el archivo de configuración del programa.
¿Desea remplazar el archivo de configuración por uno válido?
Esto restablecerá sus preferencias dentro de la aplicación.</source>
        <translation type="vanished">Something went wrong while loading program configuration file.
Would you want to replace the configuration file for a valid one?
This action will replace your application serrings.</translation>
    </message>
    <message>
        <source>&amp;Sí</source>
        <translation type="vanished">O&amp;K</translation>
    </message>
    <message>
        <source>Error al sobreescribir la configuración del programa.</source>
        <translation type="vanished">Error overwriting program configuration.</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="345"/>
        <source>&amp;Sí</source>
        <comment>positive</comment>
        <translation>Ye&amp;s</translation>
    </message>
    <message>
        <source>Error al sobreescribir la configuración del programa.
 No se puede modificar los permisos del archivo.</source>
        <translation type="vanished">Error overwriting program configuration.
File permissions can&apos;t be modified.</translation>
    </message>
    <message>
        <source>Error al cargar la configuración del programa.</source>
        <translation type="vanished">Error loading program configuration.</translation>
    </message>
    <message>
        <source>Código de error de la función: %1
Código de error: %2</source>
        <translation type="vanished">Function error code: %1
Error code: %2</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="256"/>
        <source>Debe completar correctamente los siguientes campos:</source>
        <translation>You must fill the following fields:</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="264"/>
        <source>-Nombre&lt;br&gt;</source>
        <translation>-Long name&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="273"/>
        <source>-Apellidos&lt;br&gt;</source>
        <translation>-Surname&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="284"/>
        <source>-Nombre activo&lt;br&gt;</source>
        <translation>-Forename&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="296"/>
        <source>-Día de nacimiento&lt;br&gt;</source>
        <translation>-Day of birth&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="305"/>
        <source>-Mes de nacimiento&lt;br&gt;</source>
        <translation>-Month of birth&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="314"/>
        <source>-Año de nacimiento</source>
        <translation>-Year of birth&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="338"/>
        <source>La fecha introducida es posterior a la fecha del sistema.
¿Desea continuar de todas formas?</source>
        <translation>Entered date is further than system date.
Do you want to continue anyways?</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="340"/>
        <source>Problema con la fecha introducida</source>
        <translation>Issue with the entered date</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="375"/>
        <source>No se puede continuar</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../Source/form.cpp" line="379"/>
        <source>A&amp;ceptar</source>
        <translation>O&amp;K</translation>
    </message>
</context>
<context>
    <name>License</name>
    <message>
        <location filename="../Interface/license.ui" line="20"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_license.h" line="63"/>
        <source>Acuerdo de licencia</source>
        <translation>Software license terms</translation>
    </message>
    <message>
        <location filename="../Source/license.cpp" line="48"/>
        <source>Cerrar</source>
        <translation>Close</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Aceptar</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <source>Cancelar</source>
        <translation type="vanished">Cancel</translation>
    </message>
    <message>
        <source>ERROR</source>
        <translation type="vanished">ERROR</translation>
    </message>
    <message>
        <source>Numerología Cuántica</source>
        <translation type="obsolete">Quantum numerology</translation>
    </message>
    <message>
        <source>Nombre activo:</source>
        <translation type="obsolete">Forename:</translation>
    </message>
    <message>
        <source>Sendero Natal</source>
        <translation type="obsolete">Birth path</translation>
    </message>
    <message>
        <source>Personalidad</source>
        <translation type="obsolete">Personality</translation>
    </message>
    <message>
        <source>Potencial</source>
        <translation type="obsolete">Potential</translation>
    </message>
    <message>
        <source>Vocales</source>
        <translation type="obsolete">Vowels</translation>
    </message>
    <message>
        <source>Consonantes</source>
        <translation type="obsolete">Consonants</translation>
    </message>
    <message>
        <source>N. Activo</source>
        <translation type="obsolete">Name</translation>
    </message>
</context>
<context>
    <name>Results</name>
    <message>
        <location filename="../Interface/results.ui" line="20"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="499"/>
        <source>Numerología Cuántica - Visualización de datos</source>
        <translation>Data visualization - Quantum numerology</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="665"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="534"/>
        <source>Generar plantilla</source>
        <translation>Generate image</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="672"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="535"/>
        <source>Generar documento</source>
        <translation>Generate document</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="86"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="500"/>
        <source>Sendero Natal
Personalidad
Potencial
Vocales
Consonantes
N. Activo
Año Personal</source>
        <translation>Birth path
Personality
Potential
Vowels
Consonants
Name
Personal year</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="120"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="507"/>
        <source>&lt;strong &gt;00&lt;br&gt;00&lt;br&gt;00&lt;br&gt;00&lt;br&gt;00&lt;br&gt;00&lt;br&gt;00&lt;/strong&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="148"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="508"/>
        <source>Lección de la vida
Vía de destino
Este número rige cuando se es adulto
Número del alma
Personalidad externa
Cómo vibra la persona
Desarrollo personal en el año actual</source>
        <translation>Life lesson
Destiny path
It applies when you&apos;re adult
Soul&apos;s number
External personality
How does the person vibrate
Personal development on the year</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="690"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="536"/>
        <source>NOMBRE APELLIDO APELLIDO</source>
        <translation>FULL NAME</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="719"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="537"/>
        <source>Fecha de nacimiento:</source>
        <translation>Birthdate:</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="731"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="538"/>
        <source>00/00/0000</source>
        <translation>00-00-0000</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="760"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="539"/>
        <source>Nombre activo: NOMBRE</source>
        <translation>Name in use: NAME</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="196"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="515"/>
        <source>Reducción de numeros maestros:</source>
        <translation>Master numbers reduction:</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="283"/>
        <location filename="../Interface/results.ui" line="340"/>
        <location filename="../Interface/results.ui" line="397"/>
        <location filename="../Interface/results.ui" line="454"/>
        <location filename="../Interface/results.ui" line="543"/>
        <location filename="../Interface/results.ui" line="600"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="518"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="521"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="524"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="527"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="530"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="533"/>
        <source>Reducir a 00</source>
        <translation>Reduce to 00</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="554"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="531"/>
        <location filename="../Source/results.cpp" line="292"/>
        <location filename="../Source/results.cpp" line="300"/>
        <location filename="../Source/results.cpp" line="308"/>
        <location filename="../Source/results.cpp" line="316"/>
        <source>Vocales</source>
        <translation>Vowels</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="260"/>
        <location filename="../Interface/results.ui" line="317"/>
        <location filename="../Interface/results.ui" line="374"/>
        <location filename="../Interface/results.ui" line="431"/>
        <location filename="../Interface/results.ui" line="520"/>
        <location filename="../Interface/results.ui" line="577"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="517"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="520"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="523"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="526"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="529"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="532"/>
        <source>&lt;strong&gt;00&lt;/strong&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="294"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="519"/>
        <location filename="../Source/results.cpp" line="210"/>
        <location filename="../Source/results.cpp" line="218"/>
        <source>Personalidad</source>
        <translation>Personality</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="497"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="528"/>
        <location filename="../Source/results.cpp" line="345"/>
        <location filename="../Source/results.cpp" line="353"/>
        <location filename="../Source/results.cpp" line="361"/>
        <location filename="../Source/results.cpp" line="369"/>
        <location filename="../Source/results.cpp" line="377"/>
        <source>Consonantes</source>
        <translation>Consonants</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="351"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="522"/>
        <location filename="../Source/results.cpp" line="406"/>
        <location filename="../Source/results.cpp" line="414"/>
        <location filename="../Source/results.cpp" line="422"/>
        <location filename="../Source/results.cpp" line="430"/>
        <location filename="../Source/results.cpp" line="438"/>
        <location filename="../Source/results.cpp" line="446"/>
        <source>N. Activo</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="408"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="525"/>
        <location filename="../Source/results.cpp" line="182"/>
        <source>Sendero Natal</source>
        <translation>Birth path</translation>
    </message>
    <message>
        <location filename="../Interface/results.ui" line="237"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_results.h" line="516"/>
        <location filename="../Source/results.cpp" line="247"/>
        <location filename="../Source/results.cpp" line="255"/>
        <location filename="../Source/results.cpp" line="263"/>
        <source>Potencial</source>
        <translation>Potential</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1058"/>
        <source>Guardar documento del cálculo numerológico</source>
        <translation>Save generated numerology document</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1060"/>
        <source>Documento de texto ODF</source>
        <translation>ODF text document</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1362"/>
        <source>ERROR</source>
        <translatorcomment>ERROR</translatorcomment>
        <translation>ERROR</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="52"/>
        <source>Atrás</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="53"/>
        <source>Nuevo</source>
        <translation>New</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="54"/>
        <source>Configuración</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="123"/>
        <source>Numerología Cuántica</source>
        <translation>Quantum numerology</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="146"/>
        <source>Nombre activo:</source>
        <translation>Forename:</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="475"/>
        <location filename="../Source/results.cpp" line="483"/>
        <location filename="../Source/results.cpp" line="491"/>
        <location filename="../Source/results.cpp" line="499"/>
        <location filename="../Source/results.cpp" line="507"/>
        <location filename="../Source/results.cpp" line="515"/>
        <source>Año Personal</source>
        <translation>Personal year</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="522"/>
        <source>Todos los números de la persona introducida son maestros.</source>
        <translation>All person numbers are master numbers.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="889"/>
        <source>Error al obtener un directorio temporal.
No se puede continuar.</source>
        <translation>Error when trying to get a temporal dir.
Cannot conitnue.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1033"/>
        <location filename="../Source/results.cpp" line="1052"/>
        <source>Error al guardar el documento.
No se pudo escribir los cambios en el fichero temporal.</source>
        <translation>Error saving the document.
Changes couldn&apos;t be written into the temporal file.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1056"/>
        <source>Numerología de %1 %2.odt</source>
        <translation>%1 %2 numerology.odt</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1079"/>
        <location filename="../Source/results.cpp" line="1096"/>
        <source>Error al guardar el documento.
No se puede sobrescribir el archivo.</source>
        <translation>Error saving the document.
The file couldn&apos;t be overwritten.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1138"/>
        <source>Error al guardar el documento.
Fallo al comprimir.</source>
        <translation>Error saving the document.
Compression of the file failed.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="184"/>
        <location filename="../Source/results.cpp" line="212"/>
        <location filename="../Source/results.cpp" line="220"/>
        <location filename="../Source/results.cpp" line="249"/>
        <location filename="../Source/results.cpp" line="257"/>
        <location filename="../Source/results.cpp" line="265"/>
        <location filename="../Source/results.cpp" line="294"/>
        <location filename="../Source/results.cpp" line="302"/>
        <location filename="../Source/results.cpp" line="310"/>
        <location filename="../Source/results.cpp" line="318"/>
        <location filename="../Source/results.cpp" line="347"/>
        <location filename="../Source/results.cpp" line="355"/>
        <location filename="../Source/results.cpp" line="363"/>
        <location filename="../Source/results.cpp" line="371"/>
        <location filename="../Source/results.cpp" line="379"/>
        <location filename="../Source/results.cpp" line="408"/>
        <location filename="../Source/results.cpp" line="416"/>
        <location filename="../Source/results.cpp" line="424"/>
        <location filename="../Source/results.cpp" line="432"/>
        <location filename="../Source/results.cpp" line="440"/>
        <location filename="../Source/results.cpp" line="448"/>
        <location filename="../Source/results.cpp" line="477"/>
        <location filename="../Source/results.cpp" line="485"/>
        <location filename="../Source/results.cpp" line="493"/>
        <location filename="../Source/results.cpp" line="501"/>
        <location filename="../Source/results.cpp" line="509"/>
        <location filename="../Source/results.cpp" line="517"/>
        <source>Reducir a %1</source>
        <translation>Reduce to %1</translation>
    </message>
    <message>
        <source>Los resultados del cálculo indican que todos los números de la persona introducida son maestros.&lt;br&gt;Esto podría tratarse de un poco probable error de cálculo. Este programa no fue diseñado para reducir tantos números maestros. Por ello, es necesario que seleccione si desea reducir el año personal.&lt;br&gt;&lt;strong&gt;Para reducir el año personal de un %1 a un %2&lt;/strong&gt;, deberá realizarlo de forma manual.</source>
        <translation type="vanished">All calc resulting numbers of the selected person are master numbers.&lt;br&gt;This might happen because of a calc failure.This program wasn&apos;t designed to reduce so many master numbers.&lt;br&gt;&lt;strong&gt;In order to reduce the personal year from a %1 to a %2&lt;/strong&gt;,you have to do it by yourself.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="914"/>
        <source>Error al copiar el archivo de plantilla a una carpeta temporal.</source>
        <translation>Error copying template file to a temporary folder.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="929"/>
        <source>Error al sobreescribir la plantilla de numerología.
 No se puede modificar los permisos del archivo.</source>
        <translation>Error overwriting numerological template.
File permissions can&apos;t be modified.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="947"/>
        <location filename="../Source/results.cpp" line="1000"/>
        <source>Se requiere que el programa &apos;unzip&apos; esté instalado en el sistema.
No se puede continuar.</source>
        <translation>Something wrong happened.
The program &apos;unzip&apos; has to be installed on the computer.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="966"/>
        <location filename="../Source/results.cpp" line="982"/>
        <source>Error al procesar el documento.</source>
        <translation>Error processing the numerological document.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1120"/>
        <source>&lt;strong&gt;Error al guardar el documento.&lt;/strong&gt;&lt;br&gt;Es posible que no tenga los permisos suficientes para guardar archivos en la carpeta seleccionada.</source>
        <translation>&lt;strong&gt;Error saving the document.&lt;/strong&gt;&lt;br&gt;You may not have the necessary permissions to save files in the selected folder.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1364"/>
        <source>Código de error: %1</source>
        <translation>Error code: %1</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="523"/>
        <source>Los resultados del cálculo indican que todos los números de la persona introducida son maestros.&lt;br&gt;Esto podría tratarse de un poco probable error de cálculo. Este programa no fue diseñado para reducir tantos números maestros.&lt;br&gt;&lt;strong&gt;Para reducir el año personal de un %1 a un %2&lt;/strong&gt;, deberá realizarlo de forma manual.</source>
        <translation>All calc resulting numbers of the selected person are master numbers.&lt;br&gt;This might happen because of a calc failure.This program wasn&apos;t designed to reduce so many master numbers.&lt;br&gt;&lt;strong&gt;In order to reduce the personal year from a %1 to a %2&lt;/strong&gt;,you have to do it by yourself.</translation>
    </message>
    <message>
        <source>Error al cargar la configuración del programa.</source>
        <translation type="vanished">Error loading program configuration.</translation>
    </message>
    <message>
        <source>Código de error de la función: %1
Código de error: %2</source>
        <translation type="vanished">Function error code: %1
Error code: %2</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1170"/>
        <source>&lt;strong&gt;Ha ocurrido un error al abrir el documento.&lt;/strong&gt;&lt;br&gt;No se pudo guardar el archivo en la ubicación solicitada.</source>
        <translation>&lt;strong&gt;Error opening the document.&lt;/strong&gt;&lt;br&gt;The file couldn&apos;t be saved on the indicated location.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1181"/>
        <source>&lt;strong&gt;Ha ocurrido un error al abrir el documento.&lt;/strong&gt;&lt;br&gt;Compruebe que &apos;%1&apos; esté instalado.</source>
        <translation>&lt;strong&gt;Error opening the document.&lt;/strong&gt;&lt;br&gt;Check that &apos;%1&apos; is installed on the computer.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1218"/>
        <source>Plantilla numerología de %1 %2.png</source>
        <translation>%1 %2 numerological image.png</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1223"/>
        <source>Imagen PNG</source>
        <translation>PNG Image</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1241"/>
        <location filename="../Source/results.cpp" line="1258"/>
        <source>Error al guardar la imagen. No se puede sobreescribir el archivo.</source>
        <translation>Error saving the image. The file couldn&apos;t be overwritten.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1303"/>
        <source>&lt;strong&gt;Ha ocurrido un error al abrir la imagen de la plantilla numerológica.&lt;/strong&gt;&lt;br&gt;No se pudo guardar el archivo en la ubicación solicitada.</source>
        <translation>&lt;strong&gt;Something went wrong while opening numerological image.&lt;/strong&gt;&lt;br&gt;The file couldn&apos;t be saved on the indicated location.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1313"/>
        <source>&lt;strong&gt;Ha ocurrido un error al abrir la imagen de la plantilla numerológica.&lt;/strong&gt;&lt;br&gt;Compruebe que &apos;%1&apos; esté instalado.</source>
        <translation>&lt;strong&gt;Something went wrong while opening the numerological image.&lt;/strong&gt;&lt;br&gt;Check that %1 is installed.</translation>
    </message>
    <message>
        <location filename="../Source/results.cpp" line="1221"/>
        <source>Guardar imagen de la plantilla numerológica</source>
        <translation>Save generated numerology image</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../Interface/settings.ui" line="20"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_settings.h" line="256"/>
        <source>Configuración de Numerología Cuántica</source>
        <translation>Quantum numerology settings</translation>
    </message>
    <message>
        <location filename="../Interface/settings.ui" line="37"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_settings.h" line="257"/>
        <source>Para poder aplicar la configuración, la aplicación se reiniciará.</source>
        <translation>In order to apply the configuration, the app will be restarted.</translation>
    </message>
    <message>
        <location filename="../Interface/settings.ui" line="58"/>
        <location filename="../Interface/settings.ui" line="92"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_settings.h" line="258"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_settings.h" line="264"/>
        <source>Configurar el formulario</source>
        <translation>Form settings</translation>
    </message>
    <message>
        <location filename="../Interface/settings.ui" line="99"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_settings.h" line="259"/>
        <source>Escoja el método por el cual desea introducir la fecha de nacimiento:</source>
        <translation>Choose birthdate selection method:</translation>
    </message>
    <message>
        <location filename="../Interface/settings.ui" line="119"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_settings.h" line="260"/>
        <source>Introducción manual de la fecha de nacimiento</source>
        <translation>Manual date input</translation>
    </message>
    <message>
        <location filename="../Interface/settings.ui" line="126"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_settings.h" line="261"/>
        <source>Selección de la fecha en un calendario</source>
        <translation>Date input in graphic calendar</translation>
    </message>
    <message>
        <location filename="../Interface/settings.ui" line="146"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_settings.h" line="262"/>
        <source>Abrir el documento generado tras guardar la base numerológica</source>
        <translation>Open generated numerology document after saving it</translation>
    </message>
    <message>
        <location filename="../Interface/settings.ui" line="153"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_settings.h" line="263"/>
        <source>Abrir la imagen generada tras guardar la plantilla numerológica</source>
        <translation>Open generated numerology image after saving it</translation>
    </message>
    <message>
        <location filename="../Interface/settings.ui" line="178"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_settings.h" line="270"/>
        <source>Acerca de</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../Interface/settings.ui" line="264"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_settings.h" line="267"/>
        <source>Versión 2.1.1</source>
        <translation>Version 2.1.1</translation>
    </message>
    <message>
        <location filename="../Interface/settings.ui" line="281"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_settings.h" line="268"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Programa creado para ayudar a la realización del cálculo en numerología.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Copyright (C) 2021, 2022 Anael González Paz&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Numerología Cuántica es un software libre: puede redistribuirlo y/o modificarlo bajo los términos de la GNU General Public License (Licencia Pública General de GNU) como está publicada por la Free Software Foundation (Fundación por el Software Libre), ya sea la versión 3 de la Licencia, o cualquier otra versión posterior.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Numerología Cuántica se distribuye con la intención de que sea de utilidad, pero &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;SIN NINGUNA GARANTÍA&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;; incluso sin la garantía implícita de COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO PARTICULAR. Consulte la GNU General Public License (Licencia Pública General de GNU) para más detalles.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Licencia: &lt;/span&gt;&lt;a href=&quot;#license&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#2980b9;&quot;&gt;Licencia Pública General de GNU, versión 3&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;&lt;br/&gt;Créditos: Biblioteca &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Qt&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;&lt;br/&gt;&lt;/span&gt;&lt;a href=&quot;#source&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#1d99f3;&quot;&gt;Código fuente&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Program created to help perform numerological calculation.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Copyright (C) 2021, 2022 Anael González Paz&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Numerología Cuántica (Quantum numerology) is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Numerología Cuántica is distributed in the hope that it will be useful, but &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;WITHOUT ANY WARRANTY&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;License: &lt;/span&gt;&lt;a href=&quot;#license&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#2980b9;&quot;&gt;GNU General Public License, version 3&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot;font-size:8pt;&quot;&gt;&lt;br/&gt;Credits: &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Qt&lt;/span&gt;&lt;span style=&quot;font-size:8pt&quot;&gt; library&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;&lt;br/&gt;&lt;/span&gt;&lt;a href=&quot;#source&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#1d99f3;&quot;&gt;Source code&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Programa creado para ayudar a la realización del cálculo en numerología.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Copyright (C) 2021, 2022 Anael González Paz&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Numerología Cuántica es un software libre: puede redistribuirlo y/o modificarlo bajo los términos de la GNU General Public License (Licencia Pública General de GNU) como está publicada por la Free Software Foundation (Fundación por el Software Libre), ya sea la versión 3 de la Licencia, o cualquier otra versión posterior.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Numerología Cuántica se distribuye con la intención de que sea de utilidad, pero &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;SIN NINGUNA GARANTÍA&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;; incluso sin la garantía implícita de COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO PARTICULAR. Consulte la GNU General Public License (Licencia Pública General de GNU) para más detalles.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Licencia: &lt;/span&gt;&lt;a href=&quot;#license&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#2980b9;&quot;&gt;Licencia Pública General de GNU, versión 3&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;&lt;br/&gt;Créditos: Biblioteca &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Qt&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Program created to help perform numerological calculation.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Copyright (C) 2021, 2022 Anael González Paz&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Numerología Cuántica (Quantum numerology) is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Numerología Cuántica is distributed in the hope that it will be useful, but &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;WITHOUT ANY WARRANTY&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;License: &lt;/span&gt;&lt;a href=&quot;#license&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#2980b9;&quot;&gt;GNU General Public License, version 3&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot;font-size:8pt;&quot;&gt;&lt;br/&gt;Credits: &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Qt&lt;/span&gt;&lt;span style=&quot;font-size:8pt&quot;&gt; library&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Interface/settings.ui" line="251"/>
        <location filename="../../build/src/numerologiacuantica_autogen/Interface/ui_settings.h" line="266"/>
        <source>Numerología Cuántica</source>
        <translation>Quantum numerology</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Programa creado para ayudar a la realización del cálculo en numerología.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Copyright (C) 2021 Anael González Paz&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Numerología Cuántica es un software libre: puede redistribuirlo y/o modificarlo bajo los términos de la GNU General Public License (Licencia Pública General de GNU) como está publicada por la Free Software Foundation (Fundación por el Software Libre), ya sea la versión 3 de la Licencia, o cualquier otra versión posterior.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Numerología Cuántica se distribuye con la intención de que sea de utilidad, pero &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;SIN NINGUNA GARANTÍA&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;; incluso sin la garantía implícita de COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO PARTICULAR. Consulte la GNU General Public License (Licencia Pública General de GNU) para más detalles.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Licencia: &lt;/span&gt;&lt;a href=&quot;#license&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#2980b9;&quot;&gt;Licencia Pública General de GNU, versión 3&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;&lt;br/&gt;Créditos: Biblioteca &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Qt&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Program created to help perform numerological calculation.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Copyright (C) 2021 Anael González Paz&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Numerología Cuántica (Quantum numerology) is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Numerología Cuántica is distributed in the hope that it will be useful, but &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;WITHOUT ANY WARRANTY&lt;/span&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;License: &lt;/span&gt;&lt;a href=&quot;#license&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#2980b9;&quot;&gt;GNU General Public License, version 3&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot;font-size:8pt;&quot;&gt;&lt;br/&gt;Credits: &lt;/span&gt;&lt;span style=&quot; font-size:8pt; font-weight:600;&quot;&gt;Qt&lt;/span&gt;&lt;span style=&quot;font-size:8pt&quot;&gt; library&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Source/settings.cpp" line="51"/>
        <source>Aceptar</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../Source/settings.cpp" line="52"/>
        <source>Cancelar</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../Source/settings.cpp" line="54"/>
        <source>Versión %1</source>
        <translation>Version %1</translation>
    </message>
    <message>
        <source>ERROR</source>
        <translation type="vanished">ERROR</translation>
    </message>
    <message>
        <source>Error al cargar la configuración del programa.</source>
        <translation type="vanished">Error loading program configuration.</translation>
    </message>
    <message>
        <source>Código de error de la función: %1
Código de error: %2</source>
        <translation type="vanished">Function error code: %1
Error code: %2</translation>
    </message>
</context>
<context>
    <name>ajustes</name>
    <message>
        <source>Aceptar</source>
        <translation type="obsolete">OK</translation>
    </message>
    <message>
        <source>Cancelar</source>
        <translation type="obsolete">Cancel</translation>
    </message>
    <message>
        <source>ERROR</source>
        <translation type="obsolete">ERROR</translation>
    </message>
    <message>
        <source>Error al cargar la configuración del programa.</source>
        <translation type="obsolete">Error loading program configuration.</translation>
    </message>
    <message>
        <source>Código de error de la función: %1
Código de error: %2</source>
        <translation type="obsolete">Function error code: %1
Error code: %2</translation>
    </message>
</context>
<context>
    <name>license</name>
    <message>
        <source>Cerrar</source>
        <translation type="obsolete">Close</translation>
    </message>
</context>
</TS>
