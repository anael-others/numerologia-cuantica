#include "Headers/results.hpp"
#include "Headers/form.hpp"
#include "Headers/settings.hpp"
#include "Headers/template.hpp"

/*
 * Numerología Cuántica - A program made to help people perform the calculation in numerology.
 * Copyright (C) 2021, 2022  Anael González Paz
 * This file is part of Numerología Cuántica.
 * Numerología Cuántica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Numerología Cuántica is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Numerología Cuántica.  If not, see <https://www.gnu.org/licenses/>.
 *
 * DISCLAIMER: The author of this software (Anael González Paz) is not bound by
 * the purpose of this program or the uses that may result from it.
 */

/*
 * Numerología Cuántica - Programa hecho para ayudar a la realización del cálculo en numerología.
 * Copyright (C) 2021, 2022  Anael González Paz
 * Este archivo es parte de Numerología Cuántica.
 * Numerología Cuántica es un software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la GNU General Public License (Licencia Pública General de GNU) como está publicada por
 * la Free Software Foundation ( Fundación por el Software Libre), ya sea la versión 3 de la Licencia, o
 * cualquier otra versión posterior.
 *
 * Numerología Cuántica se distribuye con la intención de que sea de utilidad,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía  incluso sin la garantía implícita de
 * COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO PARTICULAR.  Consulte la
 * GNU General Public License (Licencia Pública General de GNU) para más detalles.
 * Debería haber recibido una copia de la GNU General Public License (Licencia Pública General de GNU)
 * junto con Numerología Cuántica.  En caso contrario, consulte <https://www.gnu.org/licenses/>.
 *
 * AVISO: El autor de este software (Anael González Paz) no se vincula a
 * la finalidad de este programa ni los usos que puedan resultar de este.
 */

QTimer resizeTimer;
Results::Results(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Results)
{
    ui->setupUi(this);
        ui->menubar->addAction(tr("Atrás"), this, SLOT(goBack()) );
        ui->menubar->addAction(tr("Nuevo"), this, SLOT(clearInput()) );
        ui->menubar->addAction(tr("Configuración"),this, SLOT(openSettings()) );
        resizeTimer.setInterval(100);
        resizeTimer.setSingleShot(true);
        connect( &resizeTimer, SIGNAL(timeout()), this, SLOT(resizeDone()) );
}

Results::~Results()
{
    delete ui;
}

void Results::openSettings()
{
    Settings settings;
    settings.exec();
}

void Results::clearInput()
{
    Form * form = new Form;
    form->setGeometry(geometry());
    form->show();
    this->close();
}

void Results::formTransference(Data *dataTransference)
{
    // We pass the new values into the data Data class.
    data = dataTransference;

    c1 = 0;
    cd1 = 0;
    c2 = 0;
    cd2 = 0;
    c3 = 0;
    cd3 = 0;
    c4 = 0;
    cd4 = 0;
    c5 = 0;
    cd5 = 0;
    c6 = 0;
    cd6 = 0;
    ui->groupBox->setVisible(false);
    ui->chk_red1->setVisible(false);
    ui->chk_red2->setVisible(false);
    ui->chk_red3->setVisible(false);
    ui->chk_red4->setVisible(false);
    ui->chk_red5->setVisible(false);
    ui->chk_red6->setVisible(false);
    ui->lbl_red1->setVisible(false);
    ui->lbl_red2->setVisible(false);
    ui->lbl_red3->setVisible(false);
    ui->lbl_red4->setVisible(false);
    ui->lbl_red5->setVisible(false);
    ui->lbl_red6->setVisible(false);
    ui->lbl_redu1->setVisible(false);
    ui->lbl_redu2->setVisible(false);
    ui->lbl_redu3->setVisible(false);
    ui->lbl_redu4->setVisible(false);
    ui->lbl_redu5->setVisible(false);
    ui->lbl_redu6->setVisible(false);
    data->setSenderoNatalB(data->senderoNatal());
    data->setPotencialB(data->potencial());
    data->setPersonalidadB(data->personalidad());
    data->setVocalesB(data->vocales());
    data->setConsonantesB(data->consonantes());
    data->setNombreActivoB(data->nombreActivo());
    data->setYPersonalB(data->yPersonal());
    ui->lbl_nom_2->setText("<strong>"+data->name()+" "+data->surname()+"</strong>");
    this->setWindowTitle( data->name() + " " + data->surname() + " - " + tr("Numerología Cuántica"));
    QString dnacv = QString::number(data->birthDay());
    QString mnacv = QString::number(data->birthMonth());

    if (dnacv == "1" || dnacv == "2" || dnacv == "3" || dnacv == "4" ||
        dnacv == "5" || dnacv == "6" || dnacv == "7" || dnacv == "8" || dnacv == "9")
    {
        dnacv = "0" + dnacv;
        QString::number(data->birthDay()) = dnacv;
    }

    if (mnacv == "1" || mnacv == "2" || mnacv == "3" || mnacv == "4" ||
        mnacv == "5" || mnacv == "6" || mnacv == "7" || mnacv == "8" || mnacv == "9")
    {
        mnacv = "0" + mnacv;
        QString::number(data->birthMonth()) = mnacv;
    }

    ui->lbl_fcnv_2->setText(dnacv+"/"+mnacv+"/"+QString::number(data->birthYear()));

    if (data->hasDifferentName() == true)
    {
        ui->lbl_ac_2->setVisible(true);
        ui->lbl_ac_2->setText(tr("Nombre activo:")+" "+data->differentName());
    }
    else
    {
        ui->lbl_ac_2->setVisible(false);
    }

    ui->label->setText("<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">" +
    QString::number(data->senderoNatal()) + "<br/>" + QString::number(data->personalidad()) + "<br/>" +
    QString::number(data->potencial()) + "<br/>" + QString::number(data->vocales()) + "<br/>" +
    QString::number(data->consonantes()) + "<br/>" + QString::number(data->nombreActivo())+ "<br/>" +
    QString::number(data->yPersonal()) + "</span></p></body></html>");

    //* BEGIN Detect special numbers
    if (data->senderoNatal() == 11 || data->senderoNatal() == 22 ||
        data->senderoNatal() == 33 || data->senderoNatal() == 44)
    {
        ui->groupBox->setVisible(true);
        int reducedInt;
        switch (data->senderoNatal())
        {
            case 22:
                reducedInt = 4;
            break;
            case 33:
                reducedInt = 6;
            break;
            case 44:
                reducedInt = 8;
            break;
            default:
                reducedInt = 2;
            break;
        }
        c1 = 1;
        cd1 = reducedInt;
        ui->lbl_red1->setText(tr("Sendero Natal"));
        ui->lbl_redu1->setText("<strong>" + QString::number(data->senderoNatal()) + "</strong>");
        ui->chk_red1->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
    }
    if (data->personalidad() == 11 || data->personalidad() == 22 ||
        data->personalidad() == 33 || data->personalidad() == 44)
    {
        ui->groupBox->setVisible(true);
        int reducedInt;
        switch (data->personalidad())
        {
            case 22:
                reducedInt = 4;
            break;
            case 33:
                reducedInt = 6;
            break;
            case 44:
                reducedInt = 8;
            break;
            default:
                reducedInt = 2;
            break;
        }
        if (c1==0)
        {
            c1 = 2;
            cd1 = reducedInt;
            ui->lbl_red1->setText(tr("Personalidad"));
            ui->lbl_redu1->setText("<strong>" + QString::number(data->personalidad()) + "</strong>");
            ui->chk_red1->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else
        {
            c2 = 2;
            cd2 = reducedInt;
            ui->lbl_red2->setText(tr("Personalidad"));
            ui->lbl_redu2->setText("<strong>" + QString::number(data->personalidad()) + "</strong>");
            ui->chk_red2->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
    }
    if (data->potencial() == 11 || data->potencial() == 22 ||
        data->potencial() == 33 || data->potencial() == 44)
    {
        ui->groupBox->setVisible(true);
        int reducedInt;
        switch (data->potencial()) {
        case 22:
            reducedInt = 4;
            break;
        case 33:
            reducedInt = 6;
            break;
        case 44:
            reducedInt = 8;
            break;
        default:
            reducedInt = 2;
            break;

        }
        if (c1==0)
        {
            c1 = 3;
            cd1 = reducedInt;
            ui->lbl_red1->setText(tr("Potencial"));
            ui->lbl_redu1->setText("<strong>" + QString::number(data->potencial()) + "</strong>");
            ui->chk_red1->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c2==0)
        {
            c2 = 3;
            cd2 = reducedInt;
            ui->lbl_red2->setText(tr("Potencial"));
            ui->lbl_redu2->setText("<strong>" + QString::number(data->potencial()) + "</strong>");
            ui->chk_red2->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else
        {
            c3 = 3;
            cd3 = reducedInt;
            ui->lbl_red3->setText(tr("Potencial"));
            ui->lbl_redu3->setText("<strong>" + QString::number(data->potencial()) + "</strong>");
            ui->chk_red3->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
    }
    if (data->vocales() == 11 || data->vocales() == 22 ||
        data->vocales() == 33 || data->vocales() == 44)
    {
        ui->groupBox->setVisible(true);
        int reducedInt;
        switch (data->vocales())
        {
            case 22:
                reducedInt = 4;
            break;
            case 33:
                reducedInt = 6;
            break;
            case 44:
                reducedInt = 8;
            break;
            default:
                reducedInt = 2;
            break;
        }
        if (c1==0)
        {
            c1 = 4;
            cd1 = reducedInt;
            ui->lbl_red1->setText(tr("Vocales"));
            ui->lbl_redu1->setText("<strong>" + QString::number(data->vocales()) + "</strong>");
            ui->chk_red1->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c2==0)
        {
            c2 = 4;
            cd2 = reducedInt;
            ui->lbl_red2->setText(tr("Vocales"));
            ui->lbl_redu2->setText("<strong>" + QString::number(data->vocales()) + "</strong>");
            ui->chk_red2->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c3==0)
        {
            c3 = 4;
            cd3 = reducedInt;
            ui->lbl_red3->setText(tr("Vocales"));
            ui->lbl_redu3->setText("<strong>" + QString::number(data->vocales()) + "</strong>");
            ui->chk_red3->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else
        {
            c4 = 4;
            cd4 = reducedInt;
            ui->lbl_red4->setText(tr("Vocales"));
            ui->lbl_redu4->setText("<strong>" + QString::number(data->vocales()) + "</strong>");
            ui->chk_red4->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
    }
    if (data->consonantes() == 11 || data->consonantes() == 22 ||
        data->consonantes() == 33 || data->consonantes() == 44)
    {
        ui->groupBox->setVisible(true);
        int reducedInt;
        switch (data->consonantes())
        {
            case 22:
                reducedInt = 4;
            break;
            case 33:
                reducedInt = 6;
            break;
            case 44:
                reducedInt = 8;
            break;
            default:
                reducedInt = 2;
            break;
        }
        if (c1==0)
        {
            c1 = 5;
            cd1 = reducedInt;
            ui->lbl_red1->setText(tr("Consonantes"));
            ui->lbl_redu1->setText("<strong>" + QString::number(data->consonantes()) + "</strong>");
            ui->chk_red1->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c2==0)
        {
            c2 = 5;
            cd2 = reducedInt;
            ui->lbl_red2->setText(tr("Consonantes"));
            ui->lbl_redu2->setText("<strong>" + QString::number(data->consonantes()) + "</strong>");
            ui->chk_red2->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c3==0)
        {
            c3 = 5;
            cd3 = reducedInt;
            ui->lbl_red3->setText(tr("Consonantes"));
            ui->lbl_redu3->setText("<strong>" + QString::number(data->consonantes()) + "</strong>");
            ui->chk_red3->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c4==0)
        {
            c4 = 5;
            cd4 = reducedInt;
            ui->lbl_red4->setText(tr("Consonantes"));
            ui->lbl_redu4->setText("<strong>" + QString::number(data->consonantes()) + "</strong>");
            ui->chk_red4->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else
        {
            c5 = 5;
            cd5 = reducedInt;
            ui->lbl_red5->setText(tr("Consonantes"));
            ui->lbl_redu5->setText("<strong>" + QString::number(data->consonantes()) + "</strong>");
            ui->chk_red5->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
    }
    if (data->nombreActivo() == 11 || data->nombreActivo() == 22 ||
        data->nombreActivo() == 33 || data->nombreActivo() == 44)
    {
        ui->groupBox->setVisible(true);
        int reducedInt;
        switch (data->nombreActivo())
        {
            case 22:
                reducedInt = 4;
            break;
            case 33:
                reducedInt = 6;
            break;
            case 44:
                reducedInt = 8;
            break;
            default:
                reducedInt = 2;
            break;
        }
        if (c1==0)
        {
            c1 = 6;
            cd1 = reducedInt;
            ui->lbl_red1->setText(tr("N. Activo"));
            ui->lbl_redu1->setText("<strong>" + QString::number(data->nombreActivo()) + "</strong>");
            ui->chk_red1->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c2==0)
        {
            c2 = 6;
            cd2 = reducedInt;
            ui->lbl_red2->setText(tr("N. Activo"));
            ui->lbl_redu2->setText("<strong>" + QString::number(data->nombreActivo()) + "</strong>");
            ui->chk_red2->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c3==0)
        {
            c3 = 6;
            cd3 = reducedInt;
            ui->lbl_red3->setText(tr("N. Activo"));
            ui->lbl_redu3->setText("<strong>" + QString::number(data->nombreActivo()) + "</strong>");
            ui->chk_red3->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c4==0)
        {
            c4 = 6;
            cd4 = reducedInt;
            ui->lbl_red4->setText(tr("N. Activo"));
            ui->lbl_redu4->setText("<strong>" + QString::number(data->nombreActivo()) + "</strong>");
            ui->chk_red4->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c5==0)
        {
            c5 = 6;
            cd5 = reducedInt;
            ui->lbl_red5->setText(tr("N. Activo"));
            ui->lbl_redu5->setText("<strong>" + QString::number(data->nombreActivo()) + "</strong>");
            ui->chk_red5->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else
        {
            c6 = 6;
            cd6 = reducedInt;
            ui->lbl_red6->setText(tr("N. Activo"));
            ui->lbl_redu6->setText("<strong>" + QString::number(data->nombreActivo()) + "</strong>");
            ui->chk_red6->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
    }
    if (data->yPersonal() == 11 || data->yPersonal() == 22 ||
        data->yPersonal() == 33 || data->yPersonal() == 44)
    {
        ui->groupBox->setVisible(true);
        int reducedInt;
        switch (data->yPersonal())
        {
            case 22:
                reducedInt = 4;
            break;
            case 33:
                reducedInt = 6;
            break;
            case 44:
                reducedInt = 8;
            break;
            default:
                reducedInt = 2;
            break;
        }
        if (c1==0)
        {
            c1 = 7;
            cd1 = reducedInt;
            ui->lbl_red1->setText(tr("Año Personal"));
            ui->lbl_redu1->setText("<strong>" + QString::number(data->yPersonal()) + "</strong>");
            ui->chk_red1->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c2==0)
        {
            c2 = 7;
            cd2 = reducedInt;
            ui->lbl_red2->setText(tr("Año Personal"));
            ui->lbl_redu2->setText("<strong>" + QString::number(data->yPersonal()) + "</strong>");
            ui->chk_red2->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c3==0)
        {
            c3 = 7;
            cd3 = reducedInt;
            ui->lbl_red3->setText(tr("Año Personal"));
            ui->lbl_redu3->setText("<strong>" + QString::number(data->yPersonal()) + "</strong>");
            ui->chk_red3->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c4==0)
        {
            c4 = 7;
            cd4 = reducedInt;
            ui->lbl_red4->setText(tr("Año Personal"));
            ui->lbl_redu4->setText("<strong>" + QString::number(data->yPersonal()) + "</strong>");
            ui->chk_red4->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c5==0)
        {
            c5 = 7;
            cd5 = reducedInt;
            ui->lbl_red5->setText(tr("Año Personal"));
            ui->lbl_redu5->setText("<strong>" + QString::number(data->yPersonal()) + "</strong>");
            ui->chk_red5->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else if (c6==0)
        {
            c6 = 7;
            cd6 = reducedInt;
            ui->lbl_red6->setText(tr("Año Personal"));
            ui->lbl_redu6->setText("<strong>" + QString::number(data->yPersonal()) + "</strong>");
            ui->chk_red6->setText(tr("Reducir a %1").arg(QString::number(reducedInt)));
        }
        else
        {
            QMessageBox excesiveSpecialNumbers;
            excesiveSpecialNumbers.setWindowTitle(tr("Todos los números de la persona introducida son maestros."));
            excesiveSpecialNumbers.setText(tr("Los resultados del cálculo indican que todos los números de la persona introducida son maestros.<br>Esto podría tratarse de un poco probable error de cálculo. Este programa no fue diseñado para reducir tantos números maestros.<br><strong>Para reducir el año personal de un %1 a un %2</strong>, deberá realizarlo de forma manual.").arg(QString::number(data->yPersonal()),QString::number(reducedInt)));
            excesiveSpecialNumbers.setIcon(QMessageBox::Warning);
            excesiveSpecialNumbers.exec();
        }
    }
    //* END Detect special numbers
    if (c1 != 0)
    {
        ui->groupBox->setVisible(true);
        ui->lbl_red1->setVisible(true);
        ui->lbl_redu1->setVisible(true);
        ui->chk_red1->setVisible(true);
    }
    if (c2 != 0)
    {
        ui->lbl_red2->setVisible(true);
        ui->lbl_redu2->setVisible(true);
        ui->chk_red2->setVisible(true);
    }
    if (c3 != 0)
    {
        ui->lbl_red3->setVisible(true);
        ui->lbl_redu3->setVisible(true);
        ui->chk_red3->setVisible(true);
    }
    if (c4 != 0)
    {
        ui->lbl_red4->setVisible(true);
        ui->lbl_redu4->setVisible(true);
        ui->chk_red4->setVisible(true);
    }
    if (c5 != 0)
    {
        ui->lbl_red5->setVisible(true);
        ui->lbl_redu5->setVisible(true);
        ui->chk_red5->setVisible(true);
    }
    if (c6 != 0)
    {
        ui->lbl_red6->setVisible(true);
        ui->lbl_redu6->setVisible(true);
        ui->chk_red6->setVisible(true);
    }
    if (c1 == 0)
    {
        ui->groupBox->setVisible(false);
        ui->chk_red1->setVisible(false);
        ui->chk_red2->setVisible(false);
        ui->chk_red3->setVisible(false);
        ui->chk_red4->setVisible(false);
        ui->chk_red5->setVisible(false);
        ui->chk_red6->setVisible(false);
        ui->lbl_red1->setVisible(false);
        ui->lbl_red2->setVisible(false);
        ui->lbl_red3->setVisible(false);
        ui->lbl_red4->setVisible(false);
        ui->lbl_red5->setVisible(false);
        ui->lbl_red6->setVisible(false);
        ui->lbl_redu1->setVisible(false);
        ui->lbl_redu2->setVisible(false);
        ui->lbl_redu3->setVisible(false);
        ui->lbl_redu4->setVisible(false);
        ui->lbl_redu5->setVisible(false);
        ui->lbl_redu6->setVisible(false);
    }

}

void Results::goBack()
{
    Form * form = new Form;
    form->formTransference(data);
    form->setGeometry(geometry());
    form->show();
    this->close();
}

//* BEGIN User number reduction
void Results::on_chk_red1_stateChanged(int arg1)
{
    if (arg1 != 0)
    {
        switch (c1) {
        case 1:
            data->setSenderoNatalB(cd1);
            break;
        case 2:
            data->setPersonalidadB(cd1);
            break;
        case 3:
            data->setPotencialB(cd1);
            break;
        case 4:
            data->setVocalesB(cd1);
            break;
        case 5:
            data->setConsonantesB(cd1);
            break;
        case 6:
            data->setNombreActivoB(cd1);
            break;
        case 7:
            data->setYPersonalB(cd1);
            break;

        }
        ui->label->setText("<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">" + QString::number(data->senderoNatalB()) + "<br/>" + QString::number(data->personalidadB()) + "<br/>" + QString::number(data->potencialB()) + "<br/>" + QString::number(data->vocalesB()) + "<br/>" + QString::number(data->consonantesB()) + "<br/>" + QString::number(data->nombreActivoB())+ "<br/>" + QString::number(data->yPersonalB()) + "</span></p></body></html>");
    }
    else
    {
        switch (c1) {
        case 1:
            data->setSenderoNatalB(data->senderoNatal());
            break;
        case 2:
            data->setPersonalidadB(data->personalidad());
            break;
        case 3:
            data->setPotencialB(data->potencial());
            break;
        case 4:
            data->setVocalesB(data->vocales());
            break;
        case 5:
            data->setConsonantesB(data->consonantes());
            break;
        case 6:
            data->setNombreActivoB(data->nombreActivo());
            break;
        case 7:
            data->setYPersonalB(data->yPersonal());
            break;

        }
        ui->label->setText("<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">" + QString::number(data->senderoNatalB()) + "<br/>" + QString::number(data->personalidadB()) + "<br/>" + QString::number(data->potencialB()) + "<br/>" + QString::number(data->vocalesB()) + "<br/>" + QString::number(data->consonantesB()) + "<br/>" + QString::number(data->nombreActivoB())+ "<br/>" + QString::number(data->yPersonalB()) + "</span></p></body></html>");
    }
}

void Results::on_chk_red2_stateChanged(int arg1)
{
    if (arg1 != 0)
    {
        switch (c2) {
        case 2:
            data->setPersonalidadB(cd2);
            break;
        case 3:
            data->setPotencialB(cd2);
            break;
        case 4:
            data->setVocalesB(cd2);
            break;
        case 5:
            data->setConsonantesB(cd2);
            break;
        case 6:
            data->setNombreActivoB(cd2);
            break;
        case 7:
            data->setYPersonalB(cd2);
            break;

        }
        ui->label->setText("<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">" + QString::number(data->senderoNatalB()) + "<br/>" + QString::number(data->personalidadB()) + "<br/>" + QString::number(data->potencialB()) + "<br/>" + QString::number(data->vocalesB()) + "<br/>" + QString::number(data->consonantesB()) + "<br/>" + QString::number(data->nombreActivoB())+ "<br/>" + QString::number(data->yPersonalB()) + "</span></p></body></html>");
    }
    else
    {
        switch (c2) {
        case 2:
            data->setPersonalidadB(data->personalidad());
            break;
        case 3:
            data->setPotencialB(data->potencial());
            break;
        case 4:
            data->setVocalesB(data->vocales());
            break;
        case 5:
            data->setConsonantesB(data->consonantes());
            break;
        case 6:
            data->setNombreActivoB(data->nombreActivo());
            break;
        case 7:
            data->setYPersonalB(data->yPersonal());
            break;

        }
        ui->label->setText("<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">" + QString::number(data->senderoNatalB()) + "<br/>" + QString::number(data->personalidadB()) + "<br/>" + QString::number(data->potencialB()) + "<br/>" + QString::number(data->vocalesB()) + "<br/>" + QString::number(data->consonantesB()) + "<br/>" + QString::number(data->nombreActivoB())+ "<br/>" + QString::number(data->yPersonalB()) + "</span></p></body></html>");
    }
}

void Results::on_chk_red3_stateChanged(int arg1)
{
    if (arg1 != 0)
    {
        switch (c3) {
        case 3:
            data->setPotencialB(cd3);
            break;
        case 4:
            data->setVocalesB(cd3);
            break;
        case 5:
            data->setConsonantesB(cd3);
            break;
        case 6:
            data->setNombreActivoB(cd3);
            break;
        case 7:
            data->setYPersonalB(cd3);
            break;

        }
        ui->label->setText("<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">" + QString::number(data->senderoNatalB()) + "<br/>" + QString::number(data->personalidadB()) + "<br/>" + QString::number(data->potencialB()) + "<br/>" + QString::number(data->vocalesB()) + "<br/>" + QString::number(data->consonantesB()) + "<br/>" + QString::number(data->nombreActivoB())+ "<br/>" + QString::number(data->yPersonalB()) + "</span></p></body></html>");
    }
    else
    {
        switch (c3) {
        case 3:
            data->setPotencialB(data->potencial());
            break;
        case 4:
            data->setVocalesB(data->vocales());
            break;
        case 5:
            data->setConsonantesB(data->consonantes());
            break;
        case 6:
            data->setNombreActivoB(data->nombreActivo());
            break;
        case 7:
            data->setYPersonalB(data->yPersonal());
            break;

        }
        ui->label->setText("<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">" + QString::number(data->senderoNatalB()) + "<br/>" + QString::number(data->personalidadB()) + "<br/>" + QString::number(data->potencialB()) + "<br/>" + QString::number(data->vocalesB()) + "<br/>" + QString::number(data->consonantesB()) + "<br/>" + QString::number(data->nombreActivoB())+ "<br/>" + QString::number(data->yPersonalB()) + "</span></p></body></html>");
    }
}

void Results::on_chk_red4_stateChanged(int arg1)
{
    if (arg1 != 0)
    {
        switch (c4) {
        case 4:
            data->setVocalesB(cd4);
            break;
        case 5:
            data->setConsonantesB(cd4);
            break;
        case 6:
            data->setNombreActivoB(cd4);
            break;
        case 7:
            data->setYPersonalB(cd4);
            break;

        }
        ui->label->setText("<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">" + QString::number(data->senderoNatalB()) + "<br/>" + QString::number(data->personalidadB()) + "<br/>" + QString::number(data->potencialB()) + "<br/>" + QString::number(data->vocalesB()) + "<br/>" + QString::number(data->consonantesB()) + "<br/>" + QString::number(data->nombreActivoB())+ "<br/>" + QString::number(data->yPersonalB()) + "</span></p></body></html>");
    }
    else
    {
        switch (c4) {
        case 4:
            data->setVocalesB(data->vocales());
            break;
        case 5:
            data->setConsonantesB(data->consonantes());
            break;
        case 6:
            data->setNombreActivoB(data->nombreActivo());
            break;
        case 7:
            data->setYPersonalB(data->yPersonal());
            break;

        }
        ui->label->setText("<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">" + QString::number(data->senderoNatalB()) + "<br/>" + QString::number(data->personalidadB()) + "<br/>" + QString::number(data->potencialB()) + "<br/>" + QString::number(data->vocalesB()) + "<br/>" + QString::number(data->consonantesB()) + "<br/>" + QString::number(data->nombreActivoB())+ "<br/>" + QString::number(data->yPersonalB()) + "</span></p></body></html>");
    }
}

void Results::on_chk_red5_stateChanged(int arg1)
{
    if (arg1 != 0)
    {
        switch (c5) {
        case 5:
            data->setConsonantesB(cd5);
            break;
        case 6:
            data->setNombreActivoB(cd5);
            break;
        case 7:
            data->setYPersonalB(cd5);
            break;

        }
        ui->label->setText("<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">" + QString::number(data->senderoNatalB()) + "<br/>" + QString::number(data->personalidadB()) + "<br/>" + QString::number(data->potencialB()) + "<br/>" + QString::number(data->vocalesB()) + "<br/>" + QString::number(data->consonantesB()) + "<br/>" + QString::number(data->nombreActivoB())+ "<br/>" + QString::number(data->yPersonalB()) + "</span></p></body></html>");
    }
    else
    {
        switch (c5) {
        case 5:
            data->setConsonantesB(data->consonantes());
            break;
        case 6:
            data->setNombreActivoB(data->nombreActivo());
            break;
        case 7:
            data->setYPersonalB(data->yPersonal());
            break;

        }
        ui->label->setText("<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">" + QString::number(data->senderoNatalB()) + "<br/>" + QString::number(data->personalidadB()) + "<br/>" + QString::number(data->potencialB()) + "<br/>" + QString::number(data->vocalesB()) + "<br/>" + QString::number(data->consonantesB()) + "<br/>" + QString::number(data->nombreActivoB())+ "<br/>" + QString::number(data->yPersonalB()) + "</span></p></body></html>");
    }
}

void Results::on_chk_red6_stateChanged(int arg1)
{
    if (arg1 != 0)
    {
        switch (c6) {
        case 6:
            data->setNombreActivoB(cd6);
            break;
        case 7:
            data->setYPersonalB(cd6);
            break;

        }
        ui->label->setText("<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">" + QString::number(data->senderoNatalB()) + "<br/>" + QString::number(data->personalidadB()) + "<br/>" + QString::number(data->potencialB()) + "<br/>" + QString::number(data->vocalesB()) + "<br/>" + QString::number(data->consonantesB()) + "<br/>" + QString::number(data->nombreActivoB())+ "<br/>" + QString::number(data->yPersonalB()) + "</span></p></body></html>");
    }
    else
    {
        switch (c6) {
        case 6:
            data->setNombreActivoB(data->nombreActivo());
            break;
        case 7:
            data->setYPersonalB(data->yPersonal());
            break;

        }
        ui->label->setText("<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">" + QString::number(data->senderoNatalB()) + "<br/>" + QString::number(data->personalidadB()) + "<br/>" + QString::number(data->potencialB()) + "<br/>" + QString::number(data->vocalesB()) + "<br/>" + QString::number(data->consonantesB()) + "<br/>" + QString::number(data->nombreActivoB())+ "<br/>" + QString::number(data->yPersonalB()) + "</span></p></body></html>");
    }
}
//* END User number reduction

void Results::on_btn_generar_clicked()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    QTemporaryDir temporaryDir;
    if (temporaryDir.isValid())
    {
        tmpPath = temporaryDir.path();
    }
    else
    {
        /*  ERRORCODE No. 22:
         *
         *      The temporary directory given by QTemporaryDir is not valid.
         *   -> Check storage permission policies.
         */

        QApplication::restoreOverrideCursor();
        showErrorMessage(22, tr("Error al obtener un directorio temporal.\nNo se puede continuar."));
        return;
    }
    QString stringDocumentPath = tmpPath + "/nc_plant.odt";
    QByteArray byteTmpPath = stringDocumentPath.toLocal8Bit();
    const char *charArrayTmpPath = byteTmpPath.data();
    try
    {
        QDir qDir;
        qDir.mkpath(tmpPath+"/");
        qDir.mkpath(tmpPath+"/temp/");
        qDir.mkpath(tmpPath+"/doc/");
        QFile::remove(charArrayTmpPath);
    }
    catch (error_t)
    {
        /*  ERRORCODE No. 21:
         *
         *      The temporary dirs couldn't be created, the template file couldn't
         *      be removed or both.
         *   -> Check storage permission policies.
         *   -> The temporary dir or its contents were modified externally.
         */

        QApplication::restoreOverrideCursor();
        showErrorMessage(21, tr("Error al copiar el archivo de plantilla a una carpeta temporal."));
        return;
    }

    QFile::copy(":/user/documents/plantilla", charArrayTmpPath);
    if (!QFile(charArrayTmpPath).setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner) == true)
    {
        /*  ERRORCODE No. 31:
         *
         *      The permissions of the template file couldn't be modified.
         *   -> Check storage permission policies.
         *   -> The temporary dir or its contents were modified externally.
         */

        QApplication::restoreOverrideCursor();
        showErrorMessage(31, tr("Error al sobreescribir la plantilla de numerología.\n No se puede modificar los permisos del archivo."));
        return;
    }
    FILE *archi=NULL;
    QString stringProcessPath = "unzip -o '"+tmpPath+"/nc_plant.odt' -d '"+tmpPath+"/doc'";
    QByteArray byteProcessPath = stringProcessPath.toLocal8Bit();
    archi = popen (byteProcessPath.data(), "w");
    pclose (archi);
    if (archi == NULL)
    {
        /*  ERRORCODE No. 40:
         *
         *      'unzip' is NOT installed. This program requires it to uncompress
         *      the template file.
         *   -> Install 'unzip'.
         */

        QApplication::restoreOverrideCursor();
        showErrorMessage(40, tr("Se requiere que el programa 'unzip' esté instalado en el sistema.\nNo se puede continuar."));
        return;
    }
    else
    {
        try
        {
            QString location = tmpPath+"/doc/content.xml";
            QFile *rmFile = new QFile(location);
            if(!rmFile->remove())
            {
                /*  ERRORCODE No. 55:
                 *
                 *      Couldn't remove the original content.xml file.
                 *   -> Check storage permission policies.
                 *   -> The temporary dir or its contents were modified externally.
                 */

                QApplication::restoreOverrideCursor();
                showErrorMessage(55, tr("Error al procesar el documento."));
                return;
            }
        }
        catch (error_t)
        {
            /*  ERRORCODE No. 56:
             *
             *       Couldn't remove the original content.xml file.
             *       [UNEXPECTED WAY OF RECEIVING THIS ERROR]
             *    -> Check storage permission policies.
             *    -> The temporary dir or its contents were modified externally.
             *    -> Unusual error, it might be a system-level problem.
             */

            QApplication::restoreOverrideCursor();
            showErrorMessage(56, tr("Error al procesar el documento."));
            return;
        }
        FILE *archivi=NULL;
        QString stringProcessBPath = "unzip -o '"+tmpPath+"/nc_plant.odt' -d '"+tmpPath+"/temp' content.xml";
        QByteArray byteProcessBPath = stringProcessBPath.toLocal8Bit();
        archivi = popen (byteProcessBPath.data(), "w");
        pclose (archivi);
        if (archivi == NULL)
        {
            /*  ERRORCODE No. 40:
             *
             *      'unzip' is NOT installed. This program requires it to uncompress
             *      the template file.
             *   -> Install 'unzip'.
             */

            QApplication::restoreOverrideCursor();
            showErrorMessage(40, tr("Se requiere que el programa 'unzip' esté instalado en el sistema.\nNo se puede continuar."));
            return;
        }
        else
        {
            QString stringInputPath = tmpPath + "/temp/content.xml";
            QString stringOutputPath = tmpPath + "/doc/content.xml";
            QString contents = getFileContents(QFile(stringInputPath));
            contents.replace("##txt_nom##", data->name() + " " + data->surname());
            contents.replace("##txt_nacim##", QString::number(data->birthDay()) +
            "/" + QString::number(data->birthMonth()) + "/" +
            QString::number(data->birthYear()));
            contents.replace("##txt_sen##", QString::number(data->senderoNatalB()));
            contents.replace("##txt_pers##", QString::number(data->personalidadB()));
            contents.replace("##txt_pot##", QString::number(data->potencialB()));
            contents.replace("##txt_voc##", QString::number(data->vocalesB()));
            contents.replace("##txt_cons##", QString::number(data->consonantesB()));
            contents.replace("##txt_nact##", QString::number(data->nombreActivoB()));
            contents.replace("##txt_ykpe##", QString::number(data->yPersonalB()));
            try
            {
                QFile outputFile(stringOutputPath);
                if(!outputFile.open(QIODevice::WriteOnly | QIODevice::Text))
                {
                    /*  ERRORCODE No. 57:
                     *
                     *      Couldn't write changes to temporal file
                     *      {tmpPath+"/doc/content.xml"}
                     *   -> Check storage permission policies.
                     *   -> The temporary dir or its contents were modified externally.
                     */

                    QApplication::restoreOverrideCursor();
                    showErrorMessage(57, tr("Error al guardar el documento.\nNo se pudo escribir los cambios en el fichero temporal."));
                    outputFile.close();
                    return;
                }
                QTextStream newFileContents(&outputFile);
                newFileContents << contents;
                outputFile.close();
            }
            catch (error_t)
            {
                /*  ERRORCODE No. 57:
                 *
                 *      Couldn't write changes to temporal file
                 *      {tmpPath+"/doc/content.xml"}
                 *   -> Check storage permission policies.
                 *   -> The temporary dir or its contents were modified externally.
                 */

                QApplication::restoreOverrideCursor();
                showErrorMessage(57, tr("Error al guardar el documento.\nNo se pudo escribir los cambios en el fichero temporal."));
                return;
            }
            QApplication::restoreOverrideCursor();
            QString stringFileName = QDir::homePath() + "/" + tr("Numerología de %1 %2.odt").arg(data->name(), data->surname());
            const QString outputFileName = QFileDialog::getSaveFileName(this,
                                                                  tr("Guardar documento del cálculo numerológico"),
                                                                  stringFileName,
                                                                  tr("Documento de texto ODF")+"(*.odt)");
            if (outputFileName != NULL)
            {
                QFile *outputQFile = new QFile(outputFileName);
                if(outputQFile->exists())
                {
                    try
                    {
                        if(!outputQFile->remove())
                        {
                            /*  ERRORCODE No. 58:
                             *
                             *      Couldn't remove (in order to overwrite)
                             *      the existent document.
                             *   -> The user doesn't have write permission over the file.
                             *   -> The storage drive is Read-Only.
                             */

                            QApplication::restoreOverrideCursor();
                            showErrorMessage(58, tr("Error al guardar el documento.\nNo se puede sobrescribir el archivo."));
                            return;
                        }
                    }
                    catch (error_t)
                    {
                        /*  ERRORCODE No. 59:
                         *
                         *      Couldn't remove (in order to overwrite)
                         *      [UNEXPECTED WAY OF RECEIVING THIS ERROR]
                         *      the existent document.
                         *   -> The user doesn't have write permission over the file.
                         *   -> The storage drive is Read-Only.
                         *   -> Unusual error, it might be a system-level problem.
                         */

                        QApplication::restoreOverrideCursor();
                        showErrorMessage(59, tr("Error al guardar el documento.\nNo se puede sobrescribir el archivo."));
                        return;
                    }
                }
                try
                {
                    QApplication::setOverrideCursor(Qt::BusyCursor);
                    QProcess *process = new QProcess(this);
                    process->setProgram("zip");
                    process->setArguments({"-r", outputFileName, "."});
                    process->setWorkingDirectory(tmpPath+"/doc");
                    process->start();
                    process->waitForFinished();
                    process->close();
                    if (process->exitCode() != 0)
                    {
                        /*  ERRORCODE No. 60:
                     *
                     *      Couldn't compress the document or save it on the destination dir.
                     *   -> The user doesn't have write permission over the output dir.
                     *   -> The storage drive is Read-Only.
                     */

                    QApplication::restoreOverrideCursor();
                    showErrorMessage(60, tr("<strong>Error al guardar el documento.</strong><br>Es posible que no tenga los permisos suficientes para guardar archivos en la carpeta seleccionada."));
                    return;
                    }
                }
                catch(error_t)
                {
                    /*  ERRORCODE No. 61:
                     *
                     *      Couldn't compress the document or save it on the destination dir.
                     *   LESS LIKELY:
                     *   -> The user doesn't have write permission over the output dir.
                     *   -> The storage drive is Read-Only.
                     *   MOST LIKELY:
                     *   -> 'zip' couldn't be run. Please make sure that 'zip' is installed
                     *      in your system.
                     */

                    QApplication::restoreOverrideCursor();
                    showErrorMessage(61, tr("Error al guardar el documento.\nFallo al comprimir."));
                    return;
                }
                int settingsOpenFile = qSettings.value("openFile", 0).toInt();

                /*
                 * General configuration parameter: openFile
                 *
                 *    0 -> Neither the document nor the image will be
                 *         opened after being generated.
                 *    1 -> The document will be opened after being
                 *         generated, but the image won't.
                 *    2 -> The image will be opened after being
                 *         generated, but the document won't.
                 * >= 3 -> Both the document and the image will be
                 *         opened after being generated.
                 */

                if (settingsOpenFile == 1 || settingsOpenFile == 3)
                {
                    const QString openFileProcess = "xdg-open";
                    int processExitCode = QProcess::execute(openFileProcess, {outputFileName});
                    if (processExitCode == 2 || processExitCode == 4)
                    {
                        /*  ERRORCODE No. 32:
                         *
                         *      The file couldn't be opened by the system.
                         *   -> The file was externally modified.
                         *   -> Application bug.
                         */

                        QApplication::restoreOverrideCursor();
                        showErrorMessage(32, tr("<strong>Ha ocurrido un error al abrir el documento.</strong><br>No se pudo guardar el archivo en la ubicación solicitada."));
                    }
                    else if (processExitCode != 0)
                    {
                        /*  ERRORCODE No. 41:
                         *
                         *      'xdg-open' is NOT installed. This program requires it to launch files.
                         *   -> Install 'xdg-open'.
                         */

                        QApplication::restoreOverrideCursor();
                        showErrorMessage(41, tr("<strong>Ha ocurrido un error al abrir el documento.</strong><br>Compruebe que '%1' esté instalado.").arg(openFileProcess));
                    }
                }
                QApplication::restoreOverrideCursor();
            }
        }

    }

    QDir *rmDir = new QDir(tmpPath);
    rmDir->removeRecursively();
}

QString Results::getFileContents(QFile file)
{
    // We read the file
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return NULL;
    }
    QTextStream fileTextStream(&file);
    QString fileContents = fileTextStream.readAll();
    file.close();
    return fileContents;
}

void Results::on_btn_img_clicked()
{
    QString currentName;
    if (data->hasDifferentName() == true)
    {
        currentName = data->differentName();
    }
    else
    {
        currentName = data->name();
    }
    QString stringFileName = QDir::homePath() + "/" + tr("Plantilla numerología de %1 %2.png")
    .arg(data->name(), data->surname());
    QString outputFileName = QFileDialog::getSaveFileName(this,
                                                    tr("Guardar imagen de la plantilla numerológica"),
                                                    stringFileName,
                                                    tr("Imagen PNG")+"(*.png)");
    if (outputFileName != NULL)
    {
        QFile *outputQFile = new QFile(outputFileName);
        if(outputQFile->exists())
        {
            try
            {
                if(!outputQFile->remove())
                {
                    /*  ERRORCODE No. 58:
                     *
                     *      Couldn't remove (in order to overwrite)
                     *      the existent document.
                     *   -> The user doesn't have write permission over the file.
                     *   -> The storage drive is Read-Only.
                     */

                    showErrorMessage(58, tr("Error al guardar la imagen. No se puede sobreescribir el archivo."));
                    QApplication::restoreOverrideCursor();
                    return;
                }
            }
            catch (error_t)
            {
                /*  ERRORCODE No. 59:
                 *
                 *      Couldn't remove (in order to overwrite)
                 *      [UNEXPECTED WAY OF RECEIVING THIS ERROR]
                 *      the existent document.
                 *   -> The user doesn't have write permission over the file.
                 *   -> The storage drive is Read-Only.
                 *   -> Unusual error, it might be a system-level problem.
                 */

                showErrorMessage(59, tr("Error al guardar la imagen. No se puede sobreescribir el archivo."));
                QApplication::restoreOverrideCursor();
                return;
            }
        }
        QApplication::setOverrideCursor(Qt::WaitCursor);
        QString firstVowel = NULL;
        for (int i = 0; i <= data->name().length(); i++)
        {
            if (data->name()[i] == "A" || data->name()[i] == "E" || data->name()[i] == "I" || data->name()[i] == "O" || data->name()[i] == "U")
            {
                firstVowel += data->name()[i];
                break;
            }
        }
        Template::save(data, outputFileName, firstVowel);
        QApplication::restoreOverrideCursor();
        int settingsOpenFile = qSettings.value("openFile", 0).toInt();

        /*
         * General configuration parameter: openFile
         *
         *    0 -> Neither the document nor the image will be
         *         opened after being generated.
         *    1 -> The document will be opened after being
         *         generated, but the image won't.
         *    2 -> The image will be opened after being
         *         generated, but the document won't.
         * >= 3 -> Both the document and the image will be
         *         opened after being generated.
         */

        if (settingsOpenFile == 2 || settingsOpenFile == 3)
        {
            const QString comando_open = "xdg-open";
            int processExitCode = QProcess::execute(comando_open, {outputFileName});
            if (processExitCode == 2 || processExitCode == 4)
            {
                /*  ERRORCODE No. 32:
                 *
                 *      The file couldn't be opened by the system.
                 *   -> The file was externally modified.
                 *   -> Application bug.
                 */

                showErrorMessage(32, tr("<strong>Ha ocurrido un error al abrir la imagen de la plantilla numerológica.</strong><br>No se pudo guardar el archivo en la ubicación solicitada."));
            }
            else if (processExitCode != 0)
            {
                /*  ERRORCODE No. 41:
                 *
                 *      'xdg-open' is NOT installed. This program requires it to launch files.
                 *   -> Install 'xdg-open'.
                 */

                showErrorMessage(41, tr("<strong>Ha ocurrido un error al abrir la imagen de la plantilla numerológica.</strong><br>Compruebe que '%1' esté instalado.").arg(comando_open));
            }
        }
    }
}

void Results::resizeEvent(QResizeEvent *e)
{
  resizeTimer.start(500);
  QMainWindow::resizeEvent(e);
  resizeDone();
}

void Results::resizeDone()
{
    QFont basefont = ui->lbl_sen->font();
    const QRect baseRect = contentsRect();
    const QString baseText = ui->lbl_sen->text();
    int fontSizeGuess = qMax(1,basefont.pixelSize());
    for(;;++fontSizeGuess)
    {
        QFont testFont(basefont);
        testFont.setPixelSize(fontSizeGuess);
        const QRect fontRect = QFontMetrics(testFont).boundingRect(baseText);
        if(fontRect.height() > baseRect.height() || fontRect.width() > baseRect.width())
        {
            break;
        }
    }
    for(;fontSizeGuess > 1;--fontSizeGuess)
    {
        QFont testFont(basefont);
        testFont.setPixelSize(fontSizeGuess);
        const QRect fontRect = QFontMetrics(testFont).boundingRect(baseText);
        if(fontRect.height() <= baseRect.height() && fontRect.width() <= baseRect.width())
        {
            break;
        }
    }
    basefont.setPixelSize(fontSizeGuess);
    ui->label->setFont(basefont);
    ui->label_2->setFont(basefont);
    ui->lbl_sen->setFont(basefont);
}

void Results::showErrorMessage(int errorCode, QString errorMessage)
{
    QMessageBox messageBox;
    messageBox.setIcon(messageBox.Critical);
    messageBox.setWindowTitle(tr("ERROR"));
    messageBox.setText(errorMessage);
    messageBox.setInformativeText(tr("Código de error: %1").arg(QString::number(errorCode)));
    messageBox.exec();
}
