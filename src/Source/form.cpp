#include "Headers/form.hpp"
#include "Headers/settings.hpp"
#include "Headers/results.hpp"

/*
 * Numerología Cuántica - A program made to help people perform the calculation in numerology.
 * Copyright (C) 2021, 2022  Anael González Paz
 * This file is part of Numerología Cuántica.
 * Numerología Cuántica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Numerología Cuántica is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Numerología Cuántica.  If not, see <https://www.gnu.org/licenses/>.
 *
 * DISCLAIMER: The author of this software (Anael González Paz) is not bound by
 * the purpose of this program or the uses that may result from it.
 */

/*
 * Numerología Cuántica - Programa hecho para ayudar a la realización del cálculo en numerología.
 * Copyright (C) 2021, 2022  Anael González Paz
 * Este archivo es parte de Numerología Cuántica.
 * Numerología Cuántica es un software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la GNU General Public License (Licencia Pública General de GNU) como está publicada por
 * la Free Software Foundation ( Fundación por el Software Libre), ya sea la versión 3 de la Licencia, o
 * cualquier otra versión posterior.
 *
 * Numerología Cuántica se distribuye con la intención de que sea de utilidad,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía  incluso sin la garantía implícita de
 * COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO PARTICULAR.  Consulte la
 * GNU General Public License (Licencia Pública General de GNU) para más detalles.
 * Debería haber recibido una copia de la GNU General Public License (Licencia Pública General de GNU)
 * junto con Numerología Cuántica.  En caso contrario, consulte <https://www.gnu.org/licenses/>.
 *
 * AVISO: El autor de este software (Anael González Paz) no se vincula a
 * la finalidad de este programa ni los usos que puedan resultar de este.
 */

Form::Form(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Form)
{
    ui->setupUi(this);
    // We setup some UI elements, such as the MenuBar.
    QAction * backAction = new QAction(tr("Atrás"), this);
    ui->menubar->addAction(backAction);
    backAction->setEnabled(false);
    ui->menubar->addAction(tr("Nuevo"), this, SLOT(clearInput()) );
    ui->menubar->addAction(tr("Configuración"),this, SLOT(openSettings()) );
    ui->lbl_activo->setVisible(false);
    ui->txt_activo->setVisible(false);
    ui->txt_nacim1->setValidator(new QRegExpValidator( QRegExp("[0-9]{0,255}"), this ));
    ui->txt_nacim2->setValidator(new QRegExpValidator( QRegExp("[0-9]{0,255}"), this ));
    ui->txt_nacim3->setValidator(new QRegExpValidator( QRegExp("[0-9]{0,255}"), this ));
    ui->txt_nom->setValidator(new QRegExpValidator( QRegExp("[A-Za-z áéíóúÁÉÍÓÚñÑ_]{0,255}"), this ));
    ui->txt_apellidos->setValidator(new QRegExpValidator( QRegExp("[A-Za-z áéíóúÁÉÍÓÚñÑ_]{0,255}"), this ));
    ui->txt_activo->setValidator(new QRegExpValidator( QRegExp("[A-Za-z áéíóúÁÉÍÓÚñÑ_]{0,255}"), this ));
    ui->dateEdit_2->setDate(QDate::currentDate());
    ui->txt_nom->setFocus();
    loadForm();
}

Form::~Form()
{
    delete ui;
}

void Form::openSettings()
{
    Settings settings;
    settings.exec();
}

void Form::clearInput()
{
    ui->txt_nom->setText("");
    ui->txt_apellidos->setText("");
    ui->txt_activo->setText("");
    ui->chk_activo->setChecked(false);
    ui->txt_nacim1->setText("");
    ui->txt_nacim2->setText("");
    ui->txt_nacim3->setText("");
    ui->dateEdit_2->setDate(QDate::currentDate());
    ui->txt_nom->setFocus();
}

void Form::loadForm()
{
    move(qApp->desktop()->availableGeometry(this).center()-rect().center());
    int dateInputType = qSettings.value("inputType", false).toBool();
    if (dateInputType)
    {
        // The form will show the user a graphical Qt componentNuevo
        // to select the date of birth.

        ui->lbl_nacim_2->setVisible(false);
        ui->txt_nacim1->setVisible(false);
        ui->txt_nacim2->setVisible(false);
        ui->txt_nacim3->setVisible(false);
        ui->lbl_barra1->setVisible(false);
        ui->lbl_barra2->setVisible(false);
        ui->lbl_nacim->setVisible(true);
        ui->dateEdit_2->setVisible(true);
    }
    else
    {
        // The form will, instead of a graphical selector,
        // let the user manually type the date of birth.
        // [dd]/[MM]/[yyyy]

        ui->lbl_nacim_2->setVisible(true);
        ui->txt_nacim1->setVisible(true);
        ui->txt_nacim2->setVisible(true);
        ui->txt_nacim3->setVisible(true);
        ui->lbl_barra1->setVisible(true);
        ui->lbl_barra2->setVisible(true);
        ui->lbl_nacim->setVisible(false);
        ui->dateEdit_2->setVisible(false);
    }
}

// REQUEST #1: Write text in uppercase.
void Form::on_txt_nom_textChanged(const QString &arg1)
{
    ui->txt_nom->setText(arg1.toUpper());
}

// REQUEST #1: Write text in uppercase.
void Form::on_txt_apellidos_textChanged(const QString &arg1)
{
    ui->txt_apellidos->setText(arg1.toUpper());
}

// Validate day input.
void Form::on_txt_nacim1_textChanged(const QString &arg1)
{
    int textLength = arg1.toInt();
    if (ui->txt_nacim1->text().length() == ui->txt_nacim1->maxLength())
    {
        if (textLength <= 31 && textLength != 0)
        {
            ui->txt_nacim2->setFocus();
        }
        else
        {
            ui->txt_nacim1->setText("");
        }
    }
}

// Validate month input.
void Form::on_txt_nacim2_textChanged(const QString &arg1)
{
    int textLength = arg1.toInt();
    if (ui->txt_nacim2->text().length() == ui->txt_nacim2->maxLength())
    {
        if (textLength <= 12 && textLength != 0)
        {
            if (textLength == 02)
            {
                if (ui->txt_nacim1->text().toInt() <=29)
                {
                    ui->txt_nacim3->setFocus();
                }
                else
                {
                    ui->txt_nacim2->setText("");
                    ui->txt_nacim1->setText("");
                    ui->txt_nacim1->setFocus();
                }
            }
            else if (textLength == 4 || textLength == 6 || textLength == 9 || textLength == 11)
            {
                if (ui->txt_nacim1->text().toInt() <=30)
                {
                    ui->txt_nacim3->setFocus();
                }
                else
                {
                    ui->txt_nacim2->setText("");
                    ui->txt_nacim1->setText("");
                    ui->txt_nacim1->setFocus();
                }
            }
            else
            {
                ui->txt_nacim3->setFocus();
            }
        }
        else
        {
            ui->txt_nacim2->setText("");
        }
    }
}

// Validate year input
void Form::on_txt_nacim3_textChanged(const QString &arg1)
{
    int textLength = arg1.toInt();

    if(ui->txt_nacim3->text().length() == ui->txt_nacim3->maxLength())
    {
        if (textLength == 0)
        {
            ui->txt_nacim3->setText("");
        }
        // We check whether or not the input is a lap year.
        if(!(textLength%4==0 and textLength%100!=0))
        {
            if (ui->txt_nacim2->text().toInt() == 02)
            {
                if (ui->txt_nacim1->text().toInt() >=29)
                {
                    ui->txt_nacim3->setText("");
                    ui->txt_nacim2->setText("");
                    ui->txt_nacim1->setText("");
                    ui->txt_nacim1->setFocus();
                }
            }
        }
    }
}

// REQUEST #1: Write text in uppercase.
void Form::on_txt_activo_textChanged(const QString &arg1)
{
    ui->txt_activo->setText(arg1.toUpper());
}

void Form::on_chk_activo_stateChanged(int arg1)
{
    if (arg1 == 0)
    {
        // The person is called by their given name.
        ui->lbl_activo->setVisible(false);
        ui->txt_activo->setVisible(false);
    }
    else
    {
        // The person is called by a different name.
        ui->lbl_activo->setVisible(true);
        ui->txt_activo->setVisible(true);
    }
}

void Form::on_btn_continuar_clicked()
{
    bool checkFailed = false;
    QString errorText = "<p><strong>" + tr("Debe completar correctamente los siguientes campos:") + "</strong></p>";
    // We validate the form fields.
    if (ui->txt_nom->text().contains('A') || ui->txt_nom->text().contains("Á") || ui->txt_nom->text().contains('B') || ui->txt_nom->text().contains('C') || ui->txt_nom->text().contains('D') || ui->txt_nom->text().contains('E') || ui->txt_nom->text().contains("É") || ui->txt_nom->text().contains('F') || ui->txt_nom->text().contains('G') || ui->txt_nom->text().contains('H') || ui->txt_nom->text().contains('I') || ui->txt_nom->text().contains("Í") || ui->txt_nom->text().contains('J') || ui->txt_nom->text().contains('K') || ui->txt_nom->text().contains('L') || ui->txt_nom->text().contains('M') || ui->txt_nom->text().contains('N') || ui->txt_nom->text().contains("Ñ") || ui->txt_nom->text().contains('O') || ui->txt_nom->text().contains("Ó") || ui->txt_nom->text().contains('P') || ui->txt_nom->text().contains('Q') || ui->txt_nom->text().contains('R') || ui->txt_nom->text().contains('S') || ui->txt_nom->text().contains('U') || ui->txt_nom->text().contains("Ú") || ui->txt_nom->text().contains('V') || ui->txt_nom->text().contains('W') || ui->txt_nom->text().contains('X') || ui->txt_nom->text().contains('Y') || ui->txt_nom->text().contains('Z'))
    {
        checkFailed = false;
    }
    else
    {
        errorText.append(tr("-Nombre<br>"));
        checkFailed=true;
    }
    if (ui->txt_apellidos->text().contains('A') || ui->txt_apellidos->text().contains("Á") || ui->txt_apellidos->text().contains('B') || ui->txt_apellidos->text().contains('C') || ui->txt_apellidos->text().contains('D') || ui->txt_apellidos->text().contains('E') || ui->txt_apellidos->text().contains("É") || ui->txt_apellidos->text().contains('F') || ui->txt_apellidos->text().contains('G') || ui->txt_apellidos->text().contains('H') || ui->txt_apellidos->text().contains('I') || ui->txt_apellidos->text().contains("Í") || ui->txt_apellidos->text().contains('J') || ui->txt_apellidos->text().contains('K') || ui->txt_apellidos->text().contains('L') || ui->txt_apellidos->text().contains('M') || ui->txt_apellidos->text().contains('N') || ui->txt_apellidos->text().contains("Ñ") || ui->txt_apellidos->text().contains('O') || ui->txt_apellidos->text().contains("Ó") || ui->txt_apellidos->text().contains('P') || ui->txt_apellidos->text().contains('Q') || ui->txt_apellidos->text().contains('R') || ui->txt_apellidos->text().contains('S') || ui->txt_apellidos->text().contains('U') || ui->txt_apellidos->text().contains("Ú") || ui->txt_apellidos->text().contains('V') || ui->txt_apellidos->text().contains('W') || ui->txt_apellidos->text().contains('X') || ui->txt_apellidos->text().contains('Y') || ui->txt_apellidos->text().contains('Z'))
    {

    }
    else
    {
        errorText.append(tr("-Apellidos<br>"));
        checkFailed=true;
    }
    if (ui->chk_activo->isChecked() == true)
    {
        if (ui->txt_activo->text().contains('A') || ui->txt_activo->text().contains("Á") || ui->txt_activo->text().contains('B') || ui->txt_activo->text().contains('C') || ui->txt_activo->text().contains('D') || ui->txt_activo->text().contains('E') || ui->txt_activo->text().contains("É") || ui->txt_activo->text().contains('F') || ui->txt_activo->text().contains('G') || ui->txt_activo->text().contains('H') || ui->txt_activo->text().contains('I') || ui->txt_activo->text().contains("Í") || ui->txt_activo->text().contains('J') || ui->txt_activo->text().contains('K') || ui->txt_activo->text().contains('L') || ui->txt_activo->text().contains('M') || ui->txt_activo->text().contains('N') || ui->txt_activo->text().contains("Ñ") || ui->txt_activo->text().contains('O') || ui->txt_activo->text().contains("Ó") || ui->txt_activo->text().contains('P') || ui->txt_activo->text().contains('Q') || ui->txt_activo->text().contains('R') || ui->txt_activo->text().contains('S') || ui->txt_activo->text().contains('U') || ui->txt_activo->text().contains("Ú") || ui->txt_activo->text().contains('V') || ui->txt_activo->text().contains('W') || ui->txt_activo->text().contains('X') || ui->txt_activo->text().contains('Y') || ui->txt_activo->text().contains('Z'))
        {

        }
        else
        {
            errorText.append(tr("-Nombre activo<br>"));
            checkFailed=true;
        }
    }
    if (ui->lbl_barra1->isVisible()==true)
    {
        if (ui->txt_nacim1->text().length() == ui->txt_nacim1->maxLength())
        {

        }
        else
        {
            errorText.append(tr("-Día de nacimiento<br>"));
            checkFailed=true;
        }
        if (ui->txt_nacim2->text().length() == ui->txt_nacim2->maxLength())
        {

        }
        else
        {
            errorText.append(tr("-Mes de nacimiento<br>"));
            checkFailed=true;
        }
        if (ui->txt_nacim3->text().length() == ui->txt_nacim3->maxLength())
        {

        }
        else
        {
            errorText.append(tr("-Año de nacimiento"));
            checkFailed=true;
        }
    }
    if (!checkFailed)
    {
        if (ui->lbl_barra1->isVisible())
        {
            data->setBirthDay(ui->txt_nacim1->text().toInt());
            data->setBirthMonth(ui->txt_nacim2->text().toInt());
            data->setBirthYear(ui->txt_nacim3->text().toInt());
        }
        else
        {
            data->setBirthDay(ui->dateEdit_2->date().day());
            data->setBirthMonth(ui->dateEdit_2->date().month());
            data->setBirthYear(ui->dateEdit_2->date().year());
        }
        QDate * birthDate = new QDate;
        birthDate->setDate(data->birthYear(), data->birthMonth(), data->birthDay());
        bool dateCheck = true;
        if (birthDate->operator>(QDate::currentDate()))
        {
            QMessageBox errorText;
            errorText.setText(tr("La fecha introducida es posterior a la fecha del sistema.\n¿Desea continuar de todas formas?"));
            errorText.setIcon(errorText.Warning);
            errorText.setWindowTitle(tr("Problema con la fecha introducida"));
            QIcon ico_err;
            ico_err.addFile(":/user/images/icon_numero");
            errorText.setWindowIcon(ico_err);
            errorText.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            errorText.setButtonText(QMessageBox::Yes, tr("&Sí","positive"));
            if (errorText.exec() == QMessageBox::No)
            {
                dateCheck = false;
            }
            else
            {
                dateCheck = true;
            }
        }
        if (dateCheck)
        {
            data->setName(ui->txt_nom->text());
            data->setSurname(ui->txt_apellidos->text());
            data->setHasDifferentName(ui->chk_activo->isChecked());
            data->setDifferentName(ui->txt_activo->text());
            beginCalculating();
            Results * resultsForm = new Results;
            resultsForm->formTransference(data);
            resultsForm->setGeometry(geometry());
            resultsForm->show();
            resultsForm->resizeDone();
            this->close();
        }
    }
    else
    {
        QMessageBox errorDialog;
        errorDialog.setText(errorText);
        errorDialog.setIcon(errorDialog.Warning);
        errorDialog.setWindowTitle(tr("No se puede continuar"));
        QIcon ico_err;
        ico_err.addFile(":/user/images/icon_numero");
        errorDialog.setWindowIcon(ico_err);
        errorDialog.setButtonText(QMessageBox::Ok, tr("A&ceptar"));
        errorDialog.exec();
    }
}

void Form::formTransference(Data* dataTransference)
{
    ui->txt_nom->setText(dataTransference->name());
    ui->txt_apellidos->setText(dataTransference->surname());
    if (dataTransference->hasDifferentName())
    {
        ui->chk_activo->setChecked(true);
        ui->txt_activo->setText(dataTransference->differentName());
    }
    if (!(qSettings.value("inputType", false).toBool()))
    {
        ui->txt_nacim1->setText(QString::number(dataTransference->birthDay()));
        ui->txt_nacim2->setText(QString::number(dataTransference->birthMonth()));
        ui->txt_nacim3->setText(QString::number(dataTransference->birthYear()));
    }
    else
    {
        QDate birthDate;
        birthDate.setDate(dataTransference->birthYear(), dataTransference->birthMonth(), dataTransference->birthDay());
        ui->dateEdit_2->setDate(birthDate);
    }
    ui->txt_nom->setFocus();
}

void Form::beginCalculating()
{
    QString fullName = data->name() + data->surname();

    //* BEGIN Natal path
    int natalPath = data->birthDay() + data->birthMonth() + data->birthYear();
    data->setSenderoNatal(reduceIntegers(natalPath));
    //* END Natal path

    //* BEGIN Personality
    int personality = processStrings(fullName);
    data->setPersonalidad(reduceIntegers(personality));
    //* END Personality

    //* BEGIN Potential
    int potential = data->personalidad() + data->senderoNatal();
    data->setPotencial(reduceIntegers(potential));
    //* END Potential

    //* BEGIN Vowels
    int vowels = processVowels(fullName);
    data->setVocales(reduceIntegers(vowels));
    //* END Vowels

    //* BEGIN Consonants
    int consonants = processConsonants(fullName);
    data->setConsonantes(reduceIntegers(consonants));
    //* END Consonants

    //* BEGIN Current name
    int currentName = 0;
    if (data->hasDifferentName())
    {
        currentName = processStrings(data->differentName());
    }
    else
    {
        currentName = processStrings(data->name());
    }
    data->setNombreActivo(reduceIntegers(currentName));
    //* END Current name

    //* BEGIN Personal year
    int personalYear = 0;
    int birthDayMonth = data->birthDay() + data->birthMonth();
    if (data->birthMonth() < QDate::currentDate().month())
    {
        personalYear = birthDayMonth + QDate::currentDate().year();
    }
    else if (data->birthMonth() == QDate::currentDate().month())
    {
        if (data->birthDay() <= QDate::currentDate().day())
        {
            personalYear = birthDayMonth + QDate::currentDate().year();
        }
        else
        {
            personalYear = birthDayMonth + QDate::currentDate().year() - 1;
        }
    }
    else
    {
        personalYear = birthDayMonth + QDate::currentDate().year() - 1;
    }
    data->setYPersonal(reduceIntegers(personalYear));
    //* END Personal year
}

int Form::processStrings(QString input)
{
    int output = 0;
    for (int charNumber = 0; input.length() >= charNumber; charNumber++)
    {
        if (input[charNumber] == "A" || input[charNumber] == "Á" ||
            input[charNumber] == "J" || input[charNumber] == "S")
        {
            output++;
        }
        else if (input[charNumber] == "B" || input[charNumber] == "K" || input[charNumber] == "T")
        {
            output += 2;
        }
        else if (input[charNumber] == "C" || input[charNumber] == "L" ||
                 input[charNumber] == "U" || input[charNumber] == "Ú")
        {
            output += 3;
        }
        else if (input[charNumber] == "D" || input[charNumber] == "M" || input[charNumber] == "V")
        {
            output += 4;
        }
        else if (input[charNumber] == "E" || input[charNumber] == "É" ||
                 input[charNumber] == "N" || input[charNumber] == "Ñ" || input[charNumber] == "W")
        {
            output += 5;
        }
        else if (input[charNumber] == "F" || input[charNumber] == "O" ||
                 input[charNumber] == "Ó" || input[charNumber] == "X")
        {
            output += 6;
        }
        else if (input[charNumber] == "G" || input[charNumber] == "P" || input[charNumber] == "Y")
        {
            output += 7;
        }
        else if (input[charNumber] == "H" || input[charNumber] == "Q" || input[charNumber] == "Z")
        {
            output += 8;
        }
        else if (input[charNumber] == "I" || input[charNumber] == "Í" || input[charNumber] == "R")
        {
            output += 9;
        }
    }
    return output;
}

int Form::reduceIntegers(int input)
{
    int output = 0;
    for (;;)
    {
        if (input == 11 || input == 22 || input == 33 || input == 44)
        {
            output = input;
            break;
        }
        while (input != 0)
        {
            output += input % 10;
            input /= 10;
        }
        if (output > 9)
        {
            if (output == 11 || output == 22 || output == 33 || output == 44)
            {
                break;
            }
            else
            {
                input = output;
                output = 0;
            }
        }
        else
        {
            break;
        }
    }
    return output;
}

int Form::processVowels(QString input)
{
    int output = 0;
    for (int charNumber = 0; input.length() >= charNumber; charNumber++)
    {
        if (input[charNumber] == "A" || input[charNumber] == "Á")
        {
            output++;
        }
        else if (input[charNumber] == "U" || input[charNumber] == "Ú")
        {
            output += 3;
        }
        else if (input[charNumber] == "E" || input[charNumber] == "É")
        {
            output += 5;
        }
        else if (input[charNumber] == "O" || input[charNumber] == "Ó")
        {
            output += 6;
        }
        else if (input[charNumber] == "I" || input[charNumber] == "Í")
        {
            output += 9;
        }
    }
    return output;
}

int Form::processConsonants(QString input)
{
    int output = 0;
    for (int charNumber = 0; input.length() >= charNumber; charNumber++)
    {
        if (input[charNumber] == "J" || input[charNumber] == "S")
        {
            output++;
        }
        else if (input[charNumber] == "B" || input[charNumber] == "K" || input[charNumber] == "T")
        {
            output += 2;
        }
        else if (input[charNumber] == "C" || input[charNumber] == "L")
        {
            output += 3;
        }
        else if (input[charNumber] == "D" || input[charNumber] == "M" || input[charNumber] == "V")
        {
            output += 4;
        }
        else if (input[charNumber] == "N" || input[charNumber] == "Ñ" || input[charNumber] == "W")
        {
            output += 5;
        }
        else if (input[charNumber] == "F" ||input[charNumber] == "X")
        {
            output += 6;
        }
        else if (input[charNumber] == "G" || input[charNumber] == "P" || input[charNumber] == "Y")
        {
            output += 7;
        }
        else if (input[charNumber] == "H" || input[charNumber] == "Q" || input[charNumber] == "Z")
        {
            output += 8;
        }
        else if (input[charNumber] == "R")
        {
            output += 9;
        }
    }
    return output;
}
