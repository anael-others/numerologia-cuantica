#include "Headers/form.hpp"
#include "Headers/license.hpp"
#include "Headers/settings.hpp"

/*
 * Numerología Cuántica - A program made to help people perform the calculation in numerology.
 * Copyright (C) 2021, 2022  Anael González Paz
 * This file is part of Numerología Cuántica.
 * Numerología Cuántica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Numerología Cuántica is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Numerología Cuántica.  If not, see <https://www.gnu.org/licenses/>.
 *
 * DISCLAIMER: The author of this software (Anael González Paz) is not bound by
 * the purpose of this program or the uses that may result from it.
 */

/*
 * Numerología Cuántica - Programa hecho para ayudar a la realización del cálculo en numerología.
 * Copyright (C) 2021, 2022  Anael González Paz
 * Este archivo es parte de Numerología Cuántica.
 * Numerología Cuántica es un software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la GNU General Public License (Licencia Pública General de GNU) como está publicada por
 * la Free Software Foundation ( Fundación por el Software Libre), ya sea la versión 3 de la Licencia, o
 * cualquier otra versión posterior.
 *
 * Numerología Cuántica se distribuye con la intención de que sea de utilidad,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía  incluso sin la garantía implícita de
 * COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO PARTICULAR.  Consulte la
 * GNU General Public License (Licencia Pública General de GNU) para más detalles.
 * Debería haber recibido una copia de la GNU General Public License (Licencia Pública General de GNU)
 * junto con Numerología Cuántica.  En caso contrario, consulte <https://www.gnu.org/licenses/>.
 *
 * AVISO: El autor de este software (Anael González Paz) no se vincula a
 * la finalidad de este programa ni los usos que puedan resultar de este.
 */

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    requiresRelaunch = false;
    ui->buttonBox->button(QDialogButtonBox::Ok)->setText(tr("Aceptar"));
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText(tr("Cancelar"));
    ui->lbl_reiniciar->setVisible(false);
    ui->lbl_vers->setText(tr("Versión %1").arg(QCoreApplication::applicationVersion()));
    areSystemChanges = true;
    if (qSettings.value("inputType", false).toBool() == true)
    {
        ui->rdb_cal->setChecked(true);
        isCalendarWanted = true;
    }
    else
    {
        ui->rdb_man->setChecked(true);
        isCalendarWanted = false;
    }
    areSystemChanges = false;
    int settingsOpenFile = qSettings.value("openFile", 0).toInt();
    switch (settingsOpenFile)
    {
        case 0:
            ui->chk_openin->setChecked(false);
            ui->chk_openim->setChecked(false);
        break;
        case 1:
            ui->chk_openin->setChecked(true);
            ui->chk_openim->setChecked(false);
        break;
        case 2:
            ui->chk_openin->setChecked(false);
            ui->chk_openim->setChecked(true);
        break;
        default:
            ui->chk_openin->setChecked(true);
            ui->chk_openim->setChecked(true);
        break;
    }
}

Settings::~Settings()
{
    delete ui;
}

void Settings::on_rdb_man_clicked()
{
    if (areSystemChanges == false)
    {
        changeDetect();
    }
}

void Settings::on_rdb_cal_clicked()
{
    if (areSystemChanges == false)
    {
        changeDetect();
    }
}

void Settings::changeDetect()
{
    if (isCalendarWanted == true)
    {
        if(ui->rdb_cal->isChecked()==true)
        {
            ui->lbl_reiniciar->setVisible(false);
            requiresRelaunch = false;
        }
        else
        {
            ui->lbl_reiniciar->setVisible(true);
            requiresRelaunch = true;
        }
    }
    else
    {
        if(ui->rdb_cal->isChecked()==true)
        {
            ui->lbl_reiniciar->setVisible(true);
            requiresRelaunch = true;
        }
        else
        {
            ui->lbl_reiniciar->setVisible(false);
            requiresRelaunch = false;
        }
    }

}

void Settings::on_buttonBox_accepted()
{
    int settingsOpenFile;
    if (ui->chk_openin->isChecked()==true)
    {
        settingsOpenFile = 1;
    }
    else
    {
        settingsOpenFile = 0;
    }
    if (ui->chk_openim->isChecked()==true)
    {
        if (settingsOpenFile == 1)
        {
            settingsOpenFile = 3;
        }
        else
        {
            settingsOpenFile = 2;
        }
    }
    qSettings.setValue("inputType", ui->rdb_cal->isChecked());
    qSettings.setValue("openFile", settingsOpenFile);
    if (requiresRelaunch == true)
    {
        qApp->quit();
        QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
    }
}

void Settings::on_lbl_acercade_linkActivated(const QString &link)
{
    if(link == "#license")
    {
        License li;
        li.exec();
    }
    else if (link == "#source")
    {
        QDesktopServices::openUrl(QUrl("https://gitlab.com/anael-others/numerologia-cuantica"));
    }
}
