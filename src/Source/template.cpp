#include "Headers/template.hpp"

/*
 * Numerología Cuántica - A program made to help people perform the calculation in numerology.
 * Copyright (C) 2021, 2022  Anael González Paz
 * This file is part of Numerología Cuántica.
 * Numerología Cuántica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Numerología Cuántica is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Numerología Cuántica.  If not, see <https://www.gnu.org/licenses/>.
 *
 * DISCLAIMER: The author of this software (Anael González Paz) is not bound by
 * the purpose of this program or the uses that may result from it.
 */

/*
 * Numerología Cuántica - Programa hecho para ayudar a la realización del cálculo en numerología.
 * Copyright (C) 2021, 2022  Anael González Paz
 * Este archivo es parte de Numerología Cuántica.
 * Numerología Cuántica es un software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la GNU General Public License (Licencia Pública General de GNU) como está publicada por
 * la Free Software Foundation ( Fundación por el Software Libre), ya sea la versión 3 de la Licencia, o
 * cualquier otra versión posterior.
 *
 * Numerología Cuántica se distribuye con la intención de que sea de utilidad,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía  incluso sin la garantía implícita de
 * COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO PARTICULAR.  Consulte la
 * GNU General Public License (Licencia Pública General de GNU) para más detalles.
 * Debería haber recibido una copia de la GNU General Public License (Licencia Pública General de GNU)
 * junto con Numerología Cuántica.  En caso contrario, consulte <https://www.gnu.org/licenses/>.
 *
 * AVISO: El autor de este software (Anael González Paz) no se vincula a
 * la finalidad de este programa ni los usos que puedan resultar de este.
 */

//* BEGIN Variable declaration
    int lA = 0;
    int lB = 0;
    int lC = 0;
    int lD = 0;
    int lE = 0;
    int lF = 0;
    int lG = 0;
    int lH = 0;
    int lI = 0;
    int lJ = 0;
    int lK = 0;
    int lL = 0;
    int lM = 0;
    int lN = 0;
    int lO = 0;
    int lP = 0;
    int lQ = 0;
    int lR = 0;
    int lS = 0;
    int lT = 0;
    int lU = 0;
    int lV = 0;
    int lW = 0;
    int lX = 0;
    int lY = 0;
    int lZ = 0;
    int l1 = 0;
    int l2 = 0;
    int l3 = 0;
    int l4 = 0;
    int l5 = 0;
    int l6 = 0;
    int l7 = 0;
    int l8 = 0;
    int l9 = 0;
//* END Variable declaration

bool Template::save(Data *data, QString path, QString firstVowel)
{
    lA = 0;
    lB = 0;
    lC = 0;
    lD = 0;
    lE = 0;
    lF = 0;
    lG = 0;
    lH = 0;
    lI = 0;
    lJ = 0;
    lK = 0;
    lL = 0;
    lM = 0;
    lN = 0;
    lO = 0;
    lP = 0;
    lQ = 0;
    lR = 0;
    lS = 0;
    lT = 0;
    lU = 0;
    lV = 0;
    lW = 0;
    lX = 0;
    lY = 0;
    lZ = 0;
    l1 = 0;
    l2 = 0;
    l3 = 0;
    l4 = 0;
    l5 = 0;
    l6 = 0;
    l7 = 0;
    l8 = 0;
    l9 = 0;

    QString nombreCompleto = data->name() + " " + data->surname();

    QString birthDate = QString("%1/%2/%3")
    .arg(QString::number(data->birthDay()),
         QString::number(data->birthMonth()),
         QString::number(data->birthYear()));

    QString nombreUsado;
    QString senderoNatal;
    QString personalidad;
    QString potencial;
    QString vocales;
    QString consonantes;
    QString nombreActivo;
    QString yPersonal;

    if(data->hasDifferentName())
    {
        nombreUsado = data->differentName();
    }
    else
    {
        nombreUsado = data->name();
    }

    if(data->senderoNatal() != data->senderoNatalB())
    {
        senderoNatal = QString("%1 (%2)")
        .arg(QString::number(data->senderoNatalB()),
             QString::number(data->senderoNatal()));
    }
    else
    {
        senderoNatal = QString::number(data->senderoNatal());
    }
    if(data->personalidad() != data->personalidadB())
    {
        personalidad = QString("%1 (%2)")
        .arg(QString::number(data->personalidadB()),
             QString::number(data->personalidad()));
    }
    else
    {
        personalidad = QString::number(data->personalidad());
    }
    if(data->potencial() != data->potencialB())
    {
        potencial = QString("%1 (%2)")
        .arg(QString::number(data->potencialB()),
             QString::number(data->potencial()));
    }
    else
    {
        potencial = QString::number(data->potencial());
    }
    if(data->vocales() != data->vocalesB())
    {
        vocales = QString("%1 (%2)")
        .arg(QString::number(data->vocalesB()),
             QString::number(data->vocales()));
    }
    else
    {
        vocales = QString::number(data->vocales());
    }
    if(data->consonantes() != data->consonantesB())
    {
        consonantes = QString("%1 (%2)")
        .arg(QString::number(data->consonantesB()),
             QString::number(data->consonantes()));
    }
    else
    {
        consonantes = QString::number(data->consonantes());
    }
    if(data->nombreActivo() != data->nombreActivoB())
    {
        nombreActivo = QString("%1 [%2]")
        .arg(QString::number(data->nombreActivoB()),
             QString::number(data->nombreActivo()));
    }
    else
    {
        nombreActivo = QString::number(data->nombreActivo());
    }
    if(data->yPersonal() != data->yPersonalB())
    {
        yPersonal = QString("%1 (%2)")
        .arg(QString::number(data->yPersonalB()),
             QString::number(data->yPersonal()));
    }
    else
    {
        yPersonal = QString::number(data->yPersonal());
    }

    QString imagen;

    if (nombreCompleto.length() > 40)
    {
        imagen = ":/user/images/img_plantnumlarg";
    }
    else
    {
        imagen = ":/user/images/img_plantnum";
    }
    QImage image(imagen);
    QPainter p;
    if (!p.begin(&image))
    {
        return false;
    }

    //Obtención línea vocales:
  QString vocales_des;
  int lengo=0;
  while (lengo < nombreCompleto.length())
  {
      QString dato2 = NULL;
      dato2 += nombreCompleto[lengo];
      int result = Template::ProcesarVocalesStrings(dato2);
      if (result != 0)
      {
          vocales_des += QString::number(result);
      }
      else
      {
          vocales_des += " ";
      }
      lengo++;
  }

  //Obtención línea consonantes:
  QString consonantes_des;
  int lenga=0;
  while (lenga < nombreCompleto.length())
  {
      QString dato3 = NULL;
      dato3 += nombreCompleto[lenga];
      int result = Template::ProcesarConsonantesStrings(dato3);
      if (result != 0)
      {
          consonantes_des += QString::number(result);
      }
      else
      {
          consonantes_des += " ";
      }
      lenga++;
  }

  //Comprobación de los números omitidos
  QString nomit = NULL;
  if (l1 == 0)
  {
      nomit += '1';
  }
  if (l2 == 0)
  {
      if (nomit == NULL)
      {
          nomit += '2';
      }
      else
      {
          nomit += ", 2";
      }
  }
  if (l3 == 0)
  {
      if (nomit == NULL)
      {
          nomit += '3';
      }
      else
      {
          nomit += ", 3";
      }
  }
  if (l4 == 0)
  {
      if (nomit == NULL)
      {
          nomit += '4';
      }
      else
      {
          nomit += ", 4";
      }
  }
  if (l5 == 0)
  {
      if (nomit == NULL)
      {
          nomit += '5';
      }
      else
      {
          nomit += ", 5";
      }
  }
  if (l6 == 0)
  {
      if (nomit == NULL)
      {
          nomit += '6';
      }
      else
      {
          nomit += ", 6";
      }
  }
  if (l7 == 0)
  {
      if (nomit == NULL)
      {
          nomit += '7';
      }
      else
      {
          nomit += ", 7";
      }
  }
  if (l8 == 0)
  {
      if (nomit == NULL)
      {
          nomit += '8';
      }
      else
      {
          nomit += ", 8";
      }
  }
  if (l9 == 0)
  {
      if (nomit == NULL)
      {
          nomit += '9';
      }
      else
      {
          nomit += ", 9";
      }
  }
  QString nomit_txt;
  if (nomit.length() == 1)
  {
      nomit_txt = "Nº OMITIDO";
  }
  else if (nomit.length() == 0)
  {
      nomit_txt = "";
  }
  else
  {
      nomit_txt = "Nº OMITIDOS";
  }

  // Label column insertion:
  QString initialTab = "                ";
  p.setPen(QPen(Qt::black));
  p.setFont(QFont("Noto Sans", 10, QFont::Normal));
  p.drawText(image.rect(), Qt::AlignLeft, "\n\n\n\n" + initialTab + "Nombre y apellidos\n" +
  initialTab + "(Vocales)\n" + initialTab+"(Consonantes)\n\n\n\n" + initialTab +
  "Fecha de nacimiento\n" + initialTab+"SENDERO NATAL\n" + initialTab +
  "(Lección de vida)\n\n" + initialTab+"PERSONALIDAD\n" + initialTab +
  "(Vía del destino)\n\n" + initialTab+"POTENCIAL\n" + initialTab +
  "(Send.Natal+Person.)\n\n\n\n" + initialTab + "VOCALES (nº Alma)\n\n" +
  initialTab + "CONSONANTES\n" + initialTab + "(Personalidad externa)\n\n" + initialTab +
  "AÑO PERSONAL\n\n"+initialTab+"NOMBRE ACTIVO\n\n\n\n" + initialTab+nomit_txt+"\n\n" +
  initialTab + "1ª VOCAL NOMBRE\n\n" + initialTab+"NATALICIO");

  // (Monospaced) Data column insertion:
  QString personalTab = "                           ";
  p.setFont(QFont("Noto Sans Mono", 10, QFont::Bold));
  p.drawText(image.rect(), Qt::AlignLeft, "\n\n\n\n" + personalTab + nombreCompleto +
  "\n" + personalTab + vocales_des + "\n" + personalTab + consonantes_des);

  //Si hay nº omitidos, nº omitido o línea en blanco
  //Inserción de la columna de datos en la imágen:
  QString numberTab = "                                                              ";
  p.setFont(QFont("Noto Sans", 10, QFont::Bold));
  p.drawText(image.rect(), Qt::AlignLeft, "\n\n\n\n\n\n\n\n\n\n" + numberTab + birthDate +
  "\n" + numberTab + senderoNatal + "\n\n\n" + numberTab + personalidad + "\n\n\n" +
  numberTab + potencial + "\n\n\n\n\n" + numberTab + vocales + "\n\n" + numberTab +
  consonantes + "\n\n\n" + numberTab + yPersonal + "\n\n" + numberTab + nombreUsado +
  " (" + nombreActivo + ")\n\n\n\n" + numberTab + nomit + "\n\n" + numberTab +
  firstVowel + "\n\n" + numberTab + QString::number(data->birthDay()));
  p.setPen(QPen(QColor(157, 74, 229)));
  p.setFont(QFont("Noto Sans", 13, QFont::Bold));
  //A partir de 40 caracteres, hay que desplazar la tabla.
  //NUM_1_CIFRA:
  //NUM_A:
  if (nombreCompleto.length() > 40) //Diferencia 260px.
  {
      if (lA > 9)
      {
          p.drawText(3980, 1150, QString::number(lA));
      }
      else if (lA == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4000, 1150, QString::number(lA));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4000, 1150, QString::number(lA));
      }
      //NUM_J:
      if (lJ > 9)
      {
          p.drawText(3980, 1355, QString::number(lJ));
      }
      else if (lJ == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4000, 1355, QString::number(lJ));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4000, 1355, QString::number(lJ));
      }
      //NUM_S:
      if (lS > 9)
      {
          p.drawText(3980, 1550, QString::number(lS));
      }
      else if (lS == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4000, 1550, QString::number(lS));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4000, 1550, QString::number(lS));
      }
      //NUM_B:
      if (lB > 9)
      {
          p.drawText(4255, 1150, QString::number(lB));
      }
      else if (lB == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4275, 1150, QString::number(lB));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4275, 1150, QString::number(lB));
      }
      //NUM_K:
      if (lK > 9)
      {
          p.drawText(4255, 1355, QString::number(lK));
      }
      else if (lK == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4275, 1355, QString::number(lK));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4275, 1355, QString::number(lK));
      }
      //NUM_T:
      if (lT > 9)
      {
          p.drawText(4255, 1550, QString::number(lT));
      }
      else if (lT == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4275, 1550, QString::number(lT));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4275, 1550, QString::number(lT));
      }
      //NUM_C:
      if (lC > 9)
      {
          p.drawText(4530, 1150, QString::number(lC));
      }
      else if (lC == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4550, 1150, QString::number(lC));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4550, 1150, QString::number(lC));
      }
      //NUM_L:
      if (lL > 9)
      {
          p.drawText(4530, 1355, QString::number(lL));
      }
      else if (lL == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4550, 1355, QString::number(lL));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4550, 1355, QString::number(lL));
      }
      //NUM_U:
      if (lU > 9)
      {
          p.drawText(4530, 1550, QString::number(lU));
      }
      else if (lU == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4550, 1550, QString::number(lU));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4550, 1550, QString::number(lU));
      }
      //NUM_D:
      if (lD > 9)
      {
          p.drawText(4805, 1150, QString::number(lD));
      }
      else if (lD == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4825, 1150, QString::number(lD));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4825, 1150, QString::number(lD));
      }
      //NUM_M:
      if (lM > 9)
      {
          p.drawText(4805, 1355, QString::number(lM));
      }
      else if (lM == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4825, 1355, QString::number(lM));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4825, 1355, QString::number(lM));
      }
      //NUM_V:
      if (lV > 9)
      {
          p.drawText(4805, 1550, QString::number(lV));
      }
      else if (lV == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4825, 1550, QString::number(lV));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4825, 1550, QString::number(lV));
      }
      //NUM_E:
      if (lE > 9)
      {
          p.drawText(5080, 1150, QString::number(lE));
      }
      else if (lE == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5100, 1150, QString::number(lE));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5100, 1150, QString::number(lE));
      }
      //NUM_N:
      if (lN > 9)
      {
          p.drawText(5060, 1355, QString::number(lN));
      }
      else if (lN == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5100, 1355, QString::number(lN));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5100, 1355, QString::number(lN));
      }
      //NUM_W:
      if (lW > 9)
      {
          p.drawText(5080, 1550, QString::number(lW));
      }
      else if (lW == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5100, 1550, QString::number(lW));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5100, 1550, QString::number(lW));
      }
      //NUM_F:
      if (lF > 9)
      {
          p.drawText(5355, 1150, QString::number(lF));
      }
      else if (lF == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5375, 1150, QString::number(lF));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5375, 1150, QString::number(lF));
      }
      //NUM_O:
      if (lO > 9)
      {
          p.drawText(5355, 1355, QString::number(lO));
      }
      else if (lO == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5375, 1355, QString::number(lO));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5375, 1355, QString::number(lO));
      }
      //NUM_X:
      if (lX > 9)
      {
          p.drawText(5355, 1550, QString::number(lX));
      }
      else if (lX == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5375, 1550, QString::number(lX));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5375, 1550, QString::number(lX));
      }
      //NUM_G:
      if (lG > 9)
      {
          p.drawText(5630, 1150, QString::number(lG));
      }
      else if (lG == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5650, 1150, QString::number(lG));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5650, 1150, QString::number(lG));
      }
      //NUM_P:
      if (lP > 9)
      {
          p.drawText(5630, 1355, QString::number(lP));
      }
      else if (lP == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5650, 1355, QString::number(lP));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5650, 1355, QString::number(lP));
      }
      //NUM_Y:
      if (lY > 9)
      {
          p.drawText(5630, 1550, QString::number(lY));
      }
      else if (lY == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5650, 1550, QString::number(lY));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5650, 1550, QString::number(lY));
      }
      //NUM_H:
      if (lH > 9)
      {
          p.drawText(5905, 1150, QString::number(lH));
      }
      else if (lH == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5925, 1150, QString::number(lH));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5925, 1150, QString::number(lH));
      }
      //NUM_Q:
      if (lQ > 9)
      {
          p.drawText(5905, 1355, QString::number(lQ));
      }
      else if (lQ == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5925, 1355, QString::number(lQ));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5925, 1355, QString::number(lQ));
      }
      //NUM_Z:
      if (lZ > 9)
      {
          p.drawText(5905, 1550, QString::number(lZ));
      }
      else if (lZ == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5925, 1550, QString::number(lZ));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5925, 1550, QString::number(lZ));
      }
      //NUM_I:
      if (lI > 9)
      {
          p.drawText(6180, 1150, QString::number(lI));
      }
      else if (lI == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(6200, 1150, QString::number(lI));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(6200, 1150, QString::number(lI));
      }
      //NUM_R:
      if (lR > 9)
      {
          p.drawText(6180, 1355, QString::number(lR));
      }
      else if (lR == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(6200, 1355, QString::number(lR));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(6200, 1355, QString::number(lR));
      }
  }
  else
  {
      if (lA > 9)
      {
          p.drawText(3980, 890, QString::number(lA));
      }
      else if (lA == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4000, 890, QString::number(lA));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4000, 890, QString::number(lA));
      }
      //NUM_J:
      if (lJ > 9)
      {
          p.drawText(3980, 1095, QString::number(lJ));
      }
      else if (lJ == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4000, 1095, QString::number(lJ));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4000, 1095, QString::number(lJ));
      }
      //NUM_S:
      if (lS > 9)
      {
          p.drawText(3980, 1290, QString::number(lS));
      }
      else if (lS == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4000, 1290, QString::number(lS));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4000, 1290, QString::number(lS));
      }
      //NUM_B:
      if (lB > 9)
      {
          p.drawText(4255, 890, QString::number(lB));
      }
      else if (lB == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4275, 890, QString::number(lB));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4275, 890, QString::number(lB));
      }
      //NUM_K:
      if (lK > 9)
      {
          p.drawText(4255, 1095, QString::number(lK));
      }
      else if (lK == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4275, 1095, QString::number(lK));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4275, 1095, QString::number(lK));
      }
      //NUM_T:
      if (lT > 9)
      {
          p.drawText(4255, 1290, QString::number(lT));
      }
      else if (lT == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4275, 1290, QString::number(lT));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4275, 1290, QString::number(lT));
      }
      //NUM_C:
      if (lC > 9)
      {
          p.drawText(4530, 890, QString::number(lC));
      }
      else if (lC == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4550, 890, QString::number(lC));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4550, 890, QString::number(lC));
      }
      //NUM_L:
      if (lL > 9)
      {
          p.drawText(4530, 1095, QString::number(lL));
      }
      else if (lL == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4550, 1095, QString::number(lL));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4550, 1095, QString::number(lL));
      }
      //NUM_U:
      if (lU > 9)
      {
          p.drawText(4530, 1290, QString::number(lU));
      }
      else if (lU == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4550, 1290, QString::number(lU));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4550, 1290, QString::number(lU));
      }
      //NUM_D:
      if (lD > 9)
      {
          p.drawText(4805, 890, QString::number(lD));
      }
      else if (lD == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4825, 890, QString::number(lD));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4825, 890, QString::number(lD));
      }
      //NUM_M:
      if (lM > 9)
      {
          p.drawText(4805, 1095, QString::number(lM));
      }
      else if (lM == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4825, 1095, QString::number(lM));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4825, 1095, QString::number(lM));
      }
      //NUM_V:
      if (lV > 9)
      {
          p.drawText(4805, 1290, QString::number(lV));
      }
      else if (lV == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(4825, 1290, QString::number(lV));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(4825, 1290, QString::number(lV));
      }
      //NUM_E:
      if (lE > 9)
      {
          p.drawText(5080, 890, QString::number(lE));
      }
      else if (lE == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5100, 890, QString::number(lE));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5100, 890, QString::number(lE));
      }
      //NUM_N:
      if (lN > 9)
      {
          p.drawText(5060, 1095, QString::number(lN));
      }
      else if (lN == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5100, 1095, QString::number(lN));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5100, 1095, QString::number(lN));
      }
      //NUM_W:
      if (lW > 9)
      {
          p.drawText(5080, 1290, QString::number(lW));
      }
      else if (lW == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5100, 1290, QString::number(lW));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5100, 1290, QString::number(lW));
      }
      //NUM_F:
      if (lF > 9)
      {
          p.drawText(5355, 890, QString::number(lF));
      }
      else if (lF == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5375, 890, QString::number(lF));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5375, 890, QString::number(lF));
      }
      //NUM_O:
      if (lO > 9)
      {
          p.drawText(5355, 1095, QString::number(lO));
      }
      else if (lO == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5375, 1095, QString::number(lO));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5375, 1095, QString::number(lO));
      }
      //NUM_X:
      if (lX > 9)
      {
          p.drawText(5355, 1290, QString::number(lX));
      }
      else if (lX == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5375, 1290, QString::number(lX));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5375, 1290, QString::number(lX));
      }
      //NUM_G:
      if (lG > 9)
      {
          p.drawText(5630, 890, QString::number(lG));
      }
      else if (lG == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5650, 890, QString::number(lG));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5650, 890, QString::number(lG));
      }
      //NUM_P:
      if (lP > 9)
      {
          p.drawText(5630, 1095, QString::number(lP));
      }
      else if (lP == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5650, 1095, QString::number(lP));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5650, 1095, QString::number(lP));
      }
      //NUM_Y:
      if (lY > 9)
      {
          p.drawText(5630, 1290, QString::number(lY));
      }
      else if (lY == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5650, 1290, QString::number(lY));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5650, 1290, QString::number(lY));
      }
      //NUM_H:
      if (lH > 9)
      {
          p.drawText(5905, 890, QString::number(lH));
      }
      else if (lH == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5925, 890, QString::number(lH));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5925, 890, QString::number(lH));
      }
      //NUM_Q:
      if (lQ > 9)
      {
          p.drawText(5905, 1095, QString::number(lQ));
      }
      else if (lQ == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5925, 1095, QString::number(lQ));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5925, 1095, QString::number(lQ));
      }
      //NUM_Z:
      if (lZ > 9)
      {
          p.drawText(5905, 1290, QString::number(lZ));
      }
      else if (lZ == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(5925, 1290, QString::number(lZ));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(5925, 1290, QString::number(lZ));
      }
      //NUM_I:
      if (lI > 9)
      {
          p.drawText(6180, 890, QString::number(lI));
      }
      else if (lI == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(6200, 890, QString::number(lI));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(6200, 890, QString::number(lI));
      }
      //NUM_R:
      if (lR > 9)
      {
          p.drawText(6180, 1095, QString::number(lR));
      }
      else if (lR == 0)
      {
          p.setPen(QPen(QColor(170, 170, 170)));
          p.drawText(6200, 1095, QString::number(lR));
          p.setPen(QPen(QColor(157, 74, 229)));
      }
      else
      {
          p.drawText(6200, 1095, QString::number(lR));
      }
  }
  QPen arcos;
  arcos.setColor(Qt::black);
  arcos.setWidth(15);
  p.setPen(arcos);
  //NUM_1:
  for (int i = 0; i < l1; i++)
  {
      int posicionombreCompleto = 3210 - (i*60);
      p.drawArc ( posicionombreCompleto,3500, 150,500, 105*16, 150*16 );
  }
  //NUM_2:
  for (int i = 0; i < l2; i++)
  {
      int posicionombreCompleto = 3210 - (i*60);
      p.drawArc ( posicionombreCompleto,2850, 150,500, 105*16, 150*16 );
  }
  //NUM_3:
  for (int i = 0; i < l3; i++)
  {
      int posicionombreCompleto = 3210 - (i*60);
      p.drawArc( posicionombreCompleto,2200, 150,500, 105*16, 150*16 );
  }
  //NUM_4:
  for (int i = 0; i < l4; i++)
  {
      int posicionombreCompleto = 4380 - (i*60);
      p.drawArc ( posicionombreCompleto,3500, 150,500, 105*16, 150*16 );
  }
  //NUM_5:
  for (int i = 0; i < l5; i++)
  {
      int posicionombreCompleto = 4380 - (i*60);
      p.drawArc ( posicionombreCompleto,2850, 150,500, 105*16, 150*16 );
  }
  //NUM_6:
  for (int i = 0; i < l6; i++)
  {
      int posicionombreCompleto = 4380 - (i*60);
      p.drawArc( posicionombreCompleto,2200, 150,500, 105*16, 150*16 );
  }
  //NUM_7:
  for (int i = 0; i < l7; i++)
  {
      int posicionombreCompleto = 5550 - (i*60);
      p.drawArc ( posicionombreCompleto,3500, 150,500, 105*16, 150*16 );
  }
  //NUM_8:
  for (int i = 0; i < l8; i++)
  {
      int posicionombreCompleto = 5550 - (i*60);
      p.drawArc ( posicionombreCompleto,2850, 150,500, 105*16, 150*16 );
  }
  //NUM_9:
  for (int i = 0; i < l9; i++)
  {
      int posicionombreCompleto = 5550 - (i*60);
      p.drawArc( posicionombreCompleto,2200, 150,500, 105*16, 150*16 );
  }
  p.end();
  return image.save(path);
}

int Template::ProcesarVocalesStrings(QString input)
{
    int output = 0;
        if (input == "A" || input == "Á")
        {
            output++;
            l1++;
            lA++;
        }
        else if (input == "U" || input == "Ú")
        {
            output = 3;
            l3++;
            lU++;
        }
        else if (input == "E" || input == "É")
        {
            output = 5;
            l5++;
            lE++;
        }
        else if (input == "O" || input == "Ó")
        {
            output = 6;
            l6++;
            lO++;
        }
        else if (input == "I" || input == "Í")
        {
            output = 9;
            l9++;
            lI++;
        }
        else
        {
            output = 0;
        }
    return output;
}

int Template::ProcesarConsonantesStrings(QString input)
{
    int output = 0;
        if (input == "J")
        {
            output++;
            l1++;
            lJ++;
        }
        else if (input == "S")
        {
            output++;
            l1++;
            lS++;
        }
        else if (input == "B")
        {
            output = 2;
            l2++;
            lB++;
        }
        else if (input == "K")
        {
            output = 2;
            l2++;
            lK++;
        }
        else if (input == "T")
        {
            output = 2;
            l2++;
            lT++;
        }
        else if (input == "C")
        {
            output = 3;
            l3++;
            lC++;
        }
        else if (input == "L")
        {
            output = 3;
            l3++;
            lL++;
        }
        else if (input == "D")
        {
            output = 4;
            l4++;
            lD++;
        }
        else if (input == "M")
        {
            output = 4;
            l4++;
            lM++;
        }
        else if (input == "V")
        {
            output = 4;
            l4++;
            lV++;
        }
        else if (input == "N" || input == "Ñ")
        {
            output = 5;
            l5++;
            lN++;
        }
        else if (input == "W")
        {
            output = 5;
            l5++;
            lW++;
        }
        else if (input == "F")
        {
            output = 6;
            l6++;
            lF++;
        }
        else if (input == "X")
        {
            output = 6;
            l6++;
            lX++;
        }
        else if (input == "G")
        {
            output = 7;
            l7++;
            lG++;
        }
        else if (input == "P")
        {
            output = 7;
            l7++;
            lP++;
        }
        else if (input == "Y")
        {
            output = 7;
            l7++;
            lY++;
        }
        else if (input == "H")
        {
            output = 8;
            l8++;
            lH++;
        }
        else if (input == "Q")
        {
            output = 8;
            l8++;
            lQ++;
        }
        else if (input == "Z")
        {
            output = 8;
            l8++;
            lZ++;
        }
        else if (input == "R")
        {
            output = 9;
            l9++;
            lR++;
        }
        else
        {
            output = 0;
        }
    return output;
}
