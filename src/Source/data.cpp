#include "Headers/data.hpp"

/*
 * Numerología Cuántica - A program made to help people perform the calculation in numerology.
 * Copyright (C) 2021, 2022  Anael González Paz
 * This file is part of Numerología Cuántica.
 * Numerología Cuántica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Numerología Cuántica is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Numerología Cuántica.  If not, see <https://www.gnu.org/licenses/>.
 *
 * DISCLAIMER: The author of this software (Anael González Paz) is not bound by
 * the purpose of this program or the uses that may result from it.
 */

/*
 * Numerología Cuántica - Programa hecho para ayudar a la realización del cálculo en numerología.
 * Copyright (C) 2021, 2022  Anael González Paz
 * Este archivo es parte de Numerología Cuántica.
 * Numerología Cuántica es un software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la GNU General Public License (Licencia Pública General de GNU) como está publicada por
 * la Free Software Foundation ( Fundación por el Software Libre), ya sea la versión 3 de la Licencia, o
 * cualquier otra versión posterior.
 *
 * Numerología Cuántica se distribuye con la intención de que sea de utilidad,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía  incluso sin la garantía implícita de
 * COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO PARTICULAR.  Consulte la
 * GNU General Public License (Licencia Pública General de GNU) para más detalles.
 * Debería haber recibido una copia de la GNU General Public License (Licencia Pública General de GNU)
 * junto con Numerología Cuántica.  En caso contrario, consulte <https://www.gnu.org/licenses/>.
 *
 * AVISO: El autor de este software (Anael González Paz) no se vincula a
 * la finalidad de este programa ni los usos que puedan resultar de este.
 */

void Data::setName(QString inputName)
{
    dataName = inputName;
}

QString Data::name()
{
    return dataName;
}

void Data::setSurname(QString inputSurname)
{
    dataSurname = inputSurname;
}

QString Data::surname()
{
    return dataSurname;
}

void Data::setHasDifferentName(bool inputHasDifferentName)
{
    dataHasDifferentName = inputHasDifferentName;
}

bool Data::hasDifferentName()
{
    return dataHasDifferentName;
}

void Data::setDifferentName(QString inputDifferentName)
{
    dataDifferentName = inputDifferentName;
}

QString Data::differentName()
{
    return dataDifferentName;
}

void Data::setBirthDay(int inputBirthDay)
{
    dataBirthDay = inputBirthDay;
}

int Data::birthDay()
{
    return dataBirthDay;
}

void Data::setBirthMonth(int inputBirthMonth)
{
    dataBirthMonth = inputBirthMonth;
}

int Data::birthMonth()
{
    return dataBirthMonth;
}

void Data::setBirthYear(int inputBirthYear)
{
    dataBirthYear = inputBirthYear;
}

int Data::birthYear()
{
    return dataBirthYear;
}

void Data::setSenderoNatal(int inputSenderoNatal)
{
    dataSenderoNatal = inputSenderoNatal;
}

int Data::senderoNatal()
{
    return dataSenderoNatal;
}

void Data::setSenderoNatalB(int inputSenderoNatalB)
{
    dataSenderoNatalB = inputSenderoNatalB;
}

int Data::senderoNatalB()
{
    return dataSenderoNatalB;
}

void Data::setPersonalidad(int inputPersonalidad)
{
    dataPersonalidad = inputPersonalidad;
}

int Data::personalidad()
{
    return dataPersonalidad;
}

void Data::setPersonalidadB(int inputPersonalidadB)
{
    dataPersonalidadB = inputPersonalidadB;
}

int Data::personalidadB()
{
    return dataPersonalidadB;
}

void Data::setPotencial(int inputPotencial)
{
    dataPotencial = inputPotencial;
}

int Data::potencial()
{
    return dataPotencial;
}

void Data::setPotencialB(int inputPotencialB)
{
    dataPotencialB = inputPotencialB;
}

int Data::potencialB()
{
    return dataPotencialB;
}

void Data::setVocales(int inputVocales)
{
    dataVocales = inputVocales;
}

int Data::vocales()
{
    return dataVocales;
}

void Data::setVocalesB(int inputVocalesB)
{
    dataVocalesB = inputVocalesB;
}

int Data::vocalesB()
{
    return dataVocalesB;
}

void Data::setConsonantes(int inputConsonantes)
{
    dataConsonantes = inputConsonantes;
}

int Data::consonantes()
{
    return dataConsonantes;
}

void Data::setConsonantesB(int inputConsonantesB)
{
    dataConsonantesB = inputConsonantesB;
}

int Data::consonantesB()
{
    return dataConsonantesB;
}

void Data::setNombreActivo(int inputNombreActivo)
{
    dataNombreActivo = inputNombreActivo;
}

int Data::nombreActivo()
{
    return dataNombreActivo;
}

void Data::setNombreActivoB(int inputNombreActivoB)
{
    dataNombreActivoB = inputNombreActivoB;
}

int Data::nombreActivoB()
{
    return dataNombreActivoB;
}

void Data::setYPersonal(int inputYPersonal)
{
    dataYPersonal = inputYPersonal;
}

int Data::yPersonal()
{
    return dataYPersonal;
}

void Data::setYPersonalB(int inputYPersonalB)
{
    dataYPersonalB = inputYPersonalB;
}

int Data::yPersonalB()
{
    return dataYPersonalB;
}
