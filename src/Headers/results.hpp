#ifndef RESULTS_H
#define RESULTS_H

#include "Headers/data.hpp"
#include <QAction>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QMainWindow>
#include <QMessageBox>
#include <QProcess>
#include <QResizeEvent>
#include <QSettings>
#include <QTextStream>
#include <QTemporaryDir>
#include <QTimer>

/*
 * Numerología Cuántica - A program made to help people perform the calculation in numerology.
 * Copyright (C) 2021, 2022  Anael González Paz
 * This file is part of Numerología Cuántica.
 * Numerología Cuántica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Numerología Cuántica is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Numerología Cuántica.  If not, see <https://www.gnu.org/licenses/>.
 *
 * DISCLAIMER: The author of this software (Anael González Paz) is not bound by
 * the purpose of this program or the uses that may result from it.
 */

/*
 * Numerología Cuántica - Programa hecho para ayudar a la realización del cálculo en numerología.
 * Copyright (C) 2021, 2022  Anael González Paz
 * Este archivo es parte de Numerología Cuántica.
 * Numerología Cuántica es un software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la GNU General Public License (Licencia Pública General de GNU) como está publicada por
 * la Free Software Foundation ( Fundación por el Software Libre), ya sea la versión 3 de la Licencia, o
 * cualquier otra versión posterior.
 *
 * Numerología Cuántica se distribuye con la intención de que sea de utilidad,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía  incluso sin la garantía implícita de
 * COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO PARTICULAR.  Consulte la
 * GNU General Public License (Licencia Pública General de GNU) para más detalles.
 * Debería haber recibido una copia de la GNU General Public License (Licencia Pública General de GNU)
 * junto con Numerología Cuántica.  En caso contrario, consulte <https://www.gnu.org/licenses/>.
 *
 * AVISO: El autor de este software (Anael González Paz) no se vincula a
 * la finalidad de este programa ni los usos que puedan resultar de este.
 */

#include "../Interface/ui_results.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Results; }
QT_END_NAMESPACE

class Results : public QMainWindow
{
    Q_OBJECT

public:
    explicit Results(QWidget *parent = nullptr);
    ~Results();

    void formTransference(Data *dataTransference);

    QString getFileContents(QFile file);

    void resizeEvent(QResizeEvent *e);

    int c1 = 0;
    int cd1 = 0;
    int c2 = 0;
    int cd2 = 0;
    int c3 = 0;
    int cd3 = 0;
    int c4 = 0;
    int cd4 = 0;
    int c5 = 0;
    int cd5 = 0;
    int c6 = 0;
    int cd6 = 0;
    int oldSenderoNatal;
    int oldPersonalidad;
    int oldPotencial;
    int oldVocales;
    int oldConsonantes;
    int oldNombreActivo;
    int oldYPersonal;
    QString tmpPath;

public slots:
    void resizeDone();

private:
    Ui::Results *ui;

    QSettings qSettings;

    Data *data;

private slots:
        void openSettings();

        void clearInput();

        void goBack();

        void showErrorMessage(int errorCode, QString errorMessage);

        void on_chk_red1_stateChanged(int arg1);

        void on_chk_red2_stateChanged(int arg1);

        void on_chk_red3_stateChanged(int arg1);

        void on_chk_red4_stateChanged(int arg1);

        void on_chk_red5_stateChanged(int arg1);

        void on_chk_red6_stateChanged(int arg1);

        void on_btn_generar_clicked();

        void on_btn_img_clicked();
};

#endif // RESULTS_H
