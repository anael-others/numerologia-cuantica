#ifndef FORM_H
#define FORM_H

#include "Headers/data.hpp"
#include <QMainWindow>
#include <QSettings>
#include <QMessageBox>
#include <QDesktopWidget>

/*
 * Numerología Cuántica - A program made to help people perform the calculation in numerology.
 * Copyright (C) 2021, 2022  Anael González Paz
 * This file is part of Numerología Cuántica.
 * Numerología Cuántica is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Numerología Cuántica is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Numerología Cuántica.  If not, see <https://www.gnu.org/licenses/>.
 *
 * DISCLAIMER: The author of this software (Anael González Paz) is not bound by
 * the purpose of this program or the uses that may result from it.
 */

/*
 * Numerología Cuántica - Programa hecho para ayudar a la realización del cálculo en numerología.
 * Copyright (C) 2021, 2022  Anael González Paz
 * Este archivo es parte de Numerología Cuántica.
 * Numerología Cuántica es un software libre: puedes redistribuirlo y/o modificarlo
 * bajo los términos de la GNU General Public License (Licencia Pública General de GNU) como está publicada por
 * la Free Software Foundation ( Fundación por el Software Libre), ya sea la versión 3 de la Licencia, o
 * cualquier otra versión posterior.
 *
 * Numerología Cuántica se distribuye con la intención de que sea de utilidad,
 * pero SIN NINGUNA GARANTÍA; incluso sin la garantía  incluso sin la garantía implícita de
 * COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO PARTICULAR.  Consulte la
 * GNU General Public License (Licencia Pública General de GNU) para más detalles.
 * Debería haber recibido una copia de la GNU General Public License (Licencia Pública General de GNU)
 * junto con Numerología Cuántica.  En caso contrario, consulte <https://www.gnu.org/licenses/>.
 *
 * AVISO: El autor de este software (Anael González Paz) no se vincula a
 * la finalidad de este programa ni los usos que puedan resultar de este.
 */

#include "../Interface/ui_form.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Form; }
QT_END_NAMESPACE

class Form : public QMainWindow
{
    Q_OBJECT

public:
    Form(QWidget *parent = nullptr);
    ~Form();

    void formTransference(Data *dataTransference);

    void beginCalculating();

private slots:
    void openSettings();

    void clearInput();

    void loadForm();

    void on_txt_nom_textChanged(const QString &arg1);

    void on_txt_apellidos_textChanged(const QString &arg1);

    void on_txt_nacim1_textChanged(const QString &arg1);

    void on_txt_nacim2_textChanged(const QString &arg1);

    void on_txt_nacim3_textChanged(const QString &arg1);

    void on_txt_activo_textChanged(const QString &arg1);

    void on_chk_activo_stateChanged(int arg1);

    void on_btn_continuar_clicked();

private:
    Ui::Form *ui;

    QSettings qSettings;

    Data *data = new Data();

    int processStrings(QString input);
    int processVowels(QString input);
    int processConsonants(QString input);
    int reduceIntegers(int input);
};
#endif // FORM_H
