# Numerología Cuántica (Quantum Numerology)
Numerology assistant

## Description
This is a program which helps you to perform calculus in numerology, appart from offering the ability to export documents in ODT format or a graphical overview in which you can see the results in a more compact view.

## DISCLAIMER
The fact that I had developed this program does **NOT** imply, by any means, that I believe on the use case of this application, since I was requested by a third-party group to code it.  
Part of the conditions in order to made this program were to license it under a [free license](https://gitlab.com/anael-others/numerologia-cuantica/-/blob/main/COPYING).


## Installation

### From the Launchpad PPA (Ubuntu 20.04/22.04)

If you run *Ubuntu 20.04* or *22.04*, you can just add my PPA to your system:

```bash
sudo add-apt-repository ppa:orionn333/tools
sudo apt update
```

And then, install this program by just executing:

```bash
sudo apt install numerologiacuantica
```

### Compilating from source

First of all, you will need to make sure that you have the required dependencies to build and to run the program.

In Ubuntu based systems, you will need to previously execute the following command:

```bash
sudo apt install qtbase5-dev qtdeclarative5-dev qttools5-dev qttools5-dev-tools
```

This will install the necessary build dependencies.  
**If you aren't running Ubuntu** as your distro, you can search for this packages on your distribution respositories.

Then, download the dependencies in order for the program to run properly *(or, again, if you don't use Ubuntu, install the equivalent packages in your distro)*:

```bash
sudo apt install unzip zip libqt5gui5 libqt5widgets5 libqt5core5a xdg-utils fonts-noto-core fonts-noto-mono
```

After installing the necessary dependencies, you can proceed to compile te program, by cloning this repo by executing into the terminal:

```bash
git clone https://gitlab.com/anael-others/numerologia-cuantica.git
cd numerologia-cuantica
cmake -S . -B build
```

#### Installing the compiled source
You can install the resulting binary by running:
```bash
cd build
make
sudo cmake --install .
```

## Support and contributing
Since it was requested by a third-party group, *I will* **NOT** *PROVIDE SUPPORT* to this application, other than the one that I may provide to that specific group of people, whose requests I would try to fulfill.  
This repository, as well as the Launchpad one is subject to be *deleted* at any moment.

Anyways, I will still maintain this repo from time to time, improve the existing code, or add new features. If you would like to ask for more features, just open an Issue and, if the destination users liked it, I would try to implement it on my spare time.

You are free to fork this repo, and make any modifications, as long as they follow this [project License](https://www.gnu.org/licenses/gpl-3.0.en.html).


## License
This project is licensed under the third version of the GNU General Public License (GPL-3.0).  
[![GNU GPL-3.0](https://www.gnu.org/graphics/gplv3-127x51.png)](https://www.gnu.org/licenses/gpl-3.0.en.html)
